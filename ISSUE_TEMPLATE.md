<!-- Have you searched for similar issues on the repository?
Before submitting this issue, please visit our wiki for common ones: https://github.com/shunya/browser-ios/wiki
For more, check out our community site: https://community.shunya.com/ -->

### Description: 


### Steps to Reproduce 
  1.
  2.
  3.

**Actual result:** <!-- Add screenshots if needed -->


**Expected result:**


**Reproduces how often:** [Easily reproduced, Intermittent Issue]


**Shunya Version:** <!-- Provide full details Eg: v1.4.2(17.09.08.16) -->


**Device details:** <!-- Model type and iOS version Eg: iPhone 6s+ (iOS 10.3.3)-->


**Website problems only:**
- did you check with Shunya Shields down?
- did you check in Safari/Firefox (WkWebView-based browsers)? 


### Additional Information 
