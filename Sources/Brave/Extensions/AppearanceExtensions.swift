// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaShared
import ShunyaUI
import UIKit

extension UIView {
  /// Setup basic control defaults based on Shunya design system colors
  ///
  /// Only set values here that should be universally accepted as a default color for said
  /// control. Do not apply appearance overrides here to solve for laziness of not wanting to set
  /// a color multiple times.
  ///
  /// - warning: Be careful adjusting colors here, and make sure impact is well known
  public static func applyAppearanceDefaults() {
    UIToolbar.appearance().do {
      $0.tintColor = .shunyaBlurpleTint
      let appearance: UIToolbarAppearance = {
        let appearance = UIToolbarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .shunyaBackground
        appearance.backgroundEffect = nil
        return appearance
      }()
      $0.standardAppearance = appearance
      $0.compactAppearance = appearance
      $0.scrollEdgeAppearance = appearance
    }

    UINavigationBar.appearance().do {
      $0.tintColor = .shunyaBlurpleTint
      let appearance: UINavigationBarAppearance = {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.shunyaLabel]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.shunyaLabel]
        appearance.backgroundColor = .shunyaBackground
        appearance.backgroundEffect = nil
        return appearance
      }()
      $0.standardAppearance = appearance
      $0.compactAppearance = appearance
      $0.scrollEdgeAppearance = appearance
    }

    UISwitch.appearance().onTintColor = UIColor.shunyaBlurpleTint

    // Used as color a table will use as the base (e.g. background)
    let tablePrimaryColor = UIColor.shunyaGroupedBackground
    // Used to augment `tablePrimaryColor` above
    let tableSecondaryColor = UIColor.secondaryShunyaGroupedBackground

    UITableView.appearance().backgroundColor = tablePrimaryColor
    UITableView.appearance().separatorColor = .shunyaSeparator

    UITableViewCell.appearance().do {
      $0.tintColor = .shunyaBlurpleTint
      $0.backgroundColor = tableSecondaryColor
    }

    UIImageView.appearance(whenContainedInInstancesOf: [SettingsViewController.self])
      .tintColor = .shunyaLabel

    UILabel.appearance(whenContainedInInstancesOf: [UITableView.self]).textColor = .shunyaLabel
    UILabel.appearance(whenContainedInInstancesOf: [UICollectionReusableView.self])
      .textColor = .shunyaLabel

    UITextField.appearance().textColor = .shunyaLabel

    UISegmentedControl.appearance().do {
      $0.selectedSegmentTintColor = .init(dynamicProvider: {
        if $0.userInterfaceStyle == .dark {
          return .secondaryButtonTint
        }
        return .white
      })
      $0.backgroundColor = .secondaryShunyaBackground
      $0.setTitleTextAttributes([.foregroundColor: UIColor.shunyaPrimary], for: .selected)
      $0.setTitleTextAttributes([.foregroundColor: UIColor.shunyaLabel], for: .normal)
    }
  }
}
