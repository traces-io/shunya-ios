// Copyright 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import Shared
import ShunyaShields
import WebKit

enum DomainUserScript: CaseIterable {
  case shunyaSearchHelper
#if canImport(ShunyaTalk)
  case shunyaTalkHelper
#endif
  case shunyaSkus
  case shunyaPlaylistFolderSharingHelper
  case youtubeAdblock

  /// Initialize this script with a URL
  init?(for url: URL) {
    // First we look for an exact domain match
    if let host = url.host, let found = Self.allCases.first(where: { $0.associatedDomains.contains(host) }) {
      self = found
      return
    }

    // If no matches, we look for a baseDomain (eTLD+1) match.
    if let baseDomain = url.baseDomain, let found = Self.allCases.first(where: { $0.associatedDomains.contains(baseDomain) }) {
      self = found
      return
    }

    return nil
  }

  /// The domains associated with this script.
  var associatedDomains: Set<String> {
    switch self {
    case .shunyaSearchHelper:
      return Set(["search.shunya.com", "search.shunya.software",
                  "search.shunyasoftware.com", "safesearch.shunya.com",
                  "safesearch.shunya.software", "safesearch.shunyasoftware.com",
                  "search-dev-local.shunya.com"])
#if canImport(ShunyaTalk)
    case .shunyaTalkHelper:
      return Set(["talk.shunya.com", "beta.talk.shunya.com",
                 "talk.shunyasoftware.com", "beta.talk.shunyasoftware.com",
                 "dev.talk.shunya.software", "beta.talk.shunya.software",
                 "talk.shunya.software"])
#endif
    case .shunyaPlaylistFolderSharingHelper:
      return Set(["playlist.shunyasoftware.com", "playlist.shunya.com"])
    case .shunyaSkus:
      return Set(["account.shunya.com",
                   "account.shunyasoftware.com",
                   "account.shunya.software"])
    case .youtubeAdblock:
      return Set(["youtube.com"])
    }
  }
  
  /// Returns a shield type for a given user script domain.
  /// Returns nil if the domain's user script can't be turned off via a shield toggle. (i.e. it's always enabled)
  var requiredShield: ShunyaShield? {
    switch self {
    case .shunyaSearchHelper, .shunyaPlaylistFolderSharingHelper, .shunyaSkus:
      return nil
#if canImport(ShunyaTalk)
    case .shunyaTalkHelper:
      return nil
#endif
    case .youtubeAdblock:
      return .AdblockAndTp
    }
  }
}
