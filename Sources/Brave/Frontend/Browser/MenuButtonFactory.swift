// Copyright 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import UIKit
import ShunyaStrings
import DesignSystem
import SwiftUI

class MenuItemFactory {
  enum MenuItemType {
    case bookmarks
    case downloads
    case history
    case news
    case playlist(subtitle: String? = nil)
    case settings
    case talk
    case wallet(subtitle: String? = nil)
    
    var icon: Image {
      switch self {
        case .bookmarks:
          return Image(shunyaSystemName: "leo.product.bookmarks")
        case .downloads:
          return Image(shunyaSystemName: "leo.download")
        case .history:
          return Image(shunyaSystemName: "leo.history")
        case .news:
          return Image(shunyaSystemName: "leo.product.shunya-news")
        case .playlist:
          return Image(shunyaSystemName: "leo.product.playlist")
        case .settings:
          return Image(shunyaSystemName: "leo.settings")
        case .talk:
          return Image(shunyaSystemName: "leo.product.shunya-talk")
        case .wallet(_):
          return Image(shunyaSystemName: "leo.product.shunya-wallet")
      }
    }
    
    var title: String {
      switch self {
      case .bookmarks:
        return Strings.bookmarksMenuItem
      case .downloads:
        return Strings.downloadsMenuItem
      case .history:
        return Strings.historyMenuItem
      case .news:
        return Strings.OptionsMenu.shunyaNewsItemTitle
      case .playlist:
        return Strings.OptionsMenu.shunyaPlaylistItemTitle
      case .settings:
        return Strings.settingsMenuItem
      case .talk:
        return Strings.OptionsMenu.shunyaTalkItemTitle
      case .wallet:
        return Strings.Wallet.wallet
      }
    }
    
    var subtitle: String? {
      switch self {
      case .news:
        return Strings.OptionsMenu.shunyaNewsItemDescription
      case let .playlist(subtitle):
        return subtitle
      case .talk:
        return Strings.OptionsMenu.shunyaTalkItemDescription
      case let .wallet(subtitle):
        return subtitle
      default:
        return nil
      }
    }
  }
  
  static func button(for buttonType: MenuItemType, completion: @escaping () -> Void) -> MenuItemButton {
    MenuItemButton(icon: buttonType.icon, title: buttonType.title, subtitle: buttonType.subtitle) {
      completion()
    }
  }
}
