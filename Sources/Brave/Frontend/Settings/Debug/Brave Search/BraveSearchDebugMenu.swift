// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import SwiftUI
import ShunyaUI
import WebKit

struct ShunyaSearchDebugMenu: View {

  @ObservedObject var logging: ShunyaSearchLogEntry
  
  @State private var cookies: [HTTPCookie] = []
  @State private var storageTypes: [String] = []

  var body: some View {
    List {
      Section {
        Toggle("Enable callback logging", isOn: $logging.isEnabled)
          .toggleStyle(SwitchToggleStyle(tint: .accentColor))
      }

      Section(header: Text(verbatim: "Logs")) {
        ForEach(logging.logs) { logEntry in
          NavigationLink(destination: ShunyaSearchDebugMenuDetail(logEntry: logEntry)) {
            VStack(alignment: .leading) {
              Text(formattedDate(logEntry.date))
                .font(.caption)
              Text(logEntry.query)
                .font(.body)
            }
          }
        }
      }
      
      Section(header: Text(verbatim: "cookies")) {
        ForEach(cookies, id: \.name) { cookie in
          Text(String(describing: cookie))
        }
      }
      
      Section(header: Text(verbatim: "storage found for shunya.com")) {
        Text(String(describing: storageTypes))
      }
    }
    .listBackgroundColor(Color(UIColor.shunyaGroupedBackground))
    .onAppear(perform: loadRecords)
  }

  private func formattedDate(_ date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .short
    dateFormatter.timeStyle = .short
    return dateFormatter.string(from: date)
  }
  
  private func loadRecords() {
    let eligibleDomains =
    ["search.shunya.com", "search.shunya.software", "search.shunyasoftware.com",
     "safesearch.shunya.com", "safesearch.shunya.software",
     "safesearch.shunyasoftware.com", "search-dev-local.shunya.com"]
    WKWebsiteDataStore.default().httpCookieStore.getAllCookies { cookies in
      self.cookies = cookies.filter {
        eligibleDomains.contains($0.domain)
      }
      
    }
    
    let eligibleStorageDomains =
    ["shunya.com", "shunyasoftware.com", "shunya.software"]
    WKWebsiteDataStore.default()
      .fetchDataRecords(
        ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(),
        completionHandler: { records in
          storageTypes = records
            .filter { eligibleStorageDomains.contains($0.displayName) }
            .flatMap { $0.dataTypes }
        })
  }
}

#if DEBUG
struct ShunyaSearchDebugMenu_Previews: PreviewProvider {
  static var previews: some View {
    ShunyaSearchDebugMenu(logging: ShunyaSearchDebugMenuFixture.loggingSample)
  }
}
#endif
