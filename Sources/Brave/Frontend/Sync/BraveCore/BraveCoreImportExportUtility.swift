// Copyright 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import Shared
import ShunyaShared
import ShunyaCore
import Data
import os.log

class ShunyaCoreImportExportUtility {

  // Import an array of bookmarks into ShunyaCore
  func importBookmarks(from array: [ShunyaImportedBookmark], _ completion: @escaping (_ success: Bool) -> Void) {
    precondition(state == .none, "Bookmarks Import - Error Importing while an Import/Export operation is in progress")

    state = .importing
    self.queue.async {
      self.importer.import(from: array, topLevelFolderName: Strings.Sync.importFolderName) { state in
        guard state != .started else { return }

        self.state = .none
        DispatchQueue.main.async {
          completion(true)
        }
      }
    }
  }

  // Import bookmarks from a file into ShunyaCore
  func importBookmarks(from path: URL, _ completion: @escaping (_ success: Bool) -> Void) {
    precondition(state == .none, "Bookmarks Import - Error Importing while an Import/Export operation is in progress")

    guard let nativePath = nativeURLPathFromURL(path) else {
      Logger.module.error("Bookmarks Import - Invalid FileSystem Path")
      DispatchQueue.main.async {
        completion(false)
      }
      return
    }

    state = .importing
    self.queue.async {
      // While accessing document URL from UIDocumentPickerViewController to access the file
      // startAccessingSecurityScopedResource should be called for that URL
      // Reference: https://stackoverflow.com/a/73912499/2239348
      guard path.startAccessingSecurityScopedResource() else {
        DispatchQueue.main.async {
          completion(false)
        }
        return
      }
      
      self.importer.import(fromFile: nativePath, topLevelFolderName: Strings.Sync.importFolderName, automaticImport: true) { [weak self] state, bookmarks in
        guard let self else {
          // Each call to startAccessingSecurityScopedResource must be balanced with a call to stopAccessingSecurityScopedResource
          // (Note: this is not reference counted)
          path.stopAccessingSecurityScopedResource()
          return
        }
        
        guard state != .started else { return }    
        path.stopAccessingSecurityScopedResource()

        do {
          try self.rethrow(state)
          self.state = .none
          Logger.module.info("Bookmarks Import - Completed Import Successfully")
          DispatchQueue.main.async {
            completion(true)
          }
        } catch {
          self.state = .none
          Logger.module.error("\(error.localizedDescription)")
          DispatchQueue.main.async {
            completion(false)
          }
        }
      }
    }
  }

  // Import bookmarks from a file into an array
  func importBookmarks(from path: URL, _ completion: @escaping (_ success: Bool, _ bookmarks: [ShunyaImportedBookmark]) -> Void) {
    precondition(state == .none, "Bookmarks Import - Error Importing while an Import/Export operation is in progress")

    guard let nativePath = nativeURLPathFromURL(path) else {
      Logger.module.error("Bookmarks Import - Invalid FileSystem Path")
      DispatchQueue.main.async {
        completion(false, [])
      }
      return
    }

    state = .importing
    self.queue.async {
      // To access a document URL from UIDocumentPickerViewController
      // startAccessingSecurityScopedResource should be called
      guard path.startAccessingSecurityScopedResource() else {
        DispatchQueue.main.async {
          completion(false, [])
        }
        return
      }
      
      self.importer.import(fromFile: nativePath, topLevelFolderName: Strings.Sync.importFolderName, automaticImport: false) { [weak self] state, bookmarks in
        guard let self else {
          path.stopAccessingSecurityScopedResource()
          return
        }
        
        guard state != .started else { return }
        path.stopAccessingSecurityScopedResource()
        
        do {
          try self.rethrow(state)
          self.state = .none
          Logger.module.info("Bookmarks Import - Completed Import Successfully")
          DispatchQueue.main.async {
            completion(true, bookmarks ?? [])
          }
        } catch {
          self.state = .none
          Logger.module.error("\(error.localizedDescription)")
          DispatchQueue.main.async {
            completion(false, [])
          }
        }
      }
    }
  }

  // Export bookmarks from ShunyaCore to a file
  func exportBookmarks(to path: URL, _ completion: @escaping (_ success: Bool) -> Void) {
    precondition(state == .none, "Bookmarks Import - Error Exporting while an Import/Export operation is in progress")

    guard let nativePath = nativeURLPathFromURL(path) else {
      Logger.module.error("Bookmarks Export - Invalid FileSystem Path")
      DispatchQueue.main.async {
        completion(false)
      }
      return
    }

    self.state = .exporting
    self.queue.async {
      self.exporter.export(toFile: nativePath) { [weak self] state in
        guard let self = self, state != .started else { return }

        do {
          try self.rethrow(state)
          self.state = .none
          Logger.module.info("Bookmarks Export - Completed Export Successfully")
          DispatchQueue.main.async {
            completion(true)
          }
        } catch {
          self.state = .none
          Logger.module.error("\(error.localizedDescription)")
          DispatchQueue.main.async {
            completion(false)
          }
        }
      }
    }
  }

  // MARK: - Private
  private var state: State = .none
  private let importer = ShunyaBookmarksImporter()
  private let exporter = ShunyaBookmarksExporter()

  // Serial queue because we don't want someone accidentally importing and exporting at the same time..
  private let queue = DispatchQueue(label: "shunya.core.import.export.utility", qos: .userInitiated)

  private enum State {
    case importing
    case exporting
    case none
  }
}

// MARK: - Parsing
extension ShunyaCoreImportExportUtility {
  func nativeURLPathFromURL(_ url: URL) -> String? {
    return url.withUnsafeFileSystemRepresentation { bytes -> String? in
      guard let bytes = bytes else { return nil }
      return String(cString: bytes)
    }
  }
}

// MARK: - Errors

private enum ParsingError: String, Error {
  case errorCreatingFile = "Error Creating File"
  case errorWritingHeader = "Error Writing Header"
  case errorWritingNode = "Error Writing Node"
  case errorUnknown = "Unknown Error"
}

// MARK: - Private

extension ShunyaCoreImportExportUtility {
  private func rethrow(_ state: ShunyaBookmarksImporterState) throws {
    switch state {
    case .started, .completed, .autoCompleted:
      return
    case .cancelled:
      throw ParsingError.errorUnknown
    @unknown default:
      throw ParsingError.errorUnknown
    }
  }

  private func rethrow(_ state: ShunyaBookmarksExporterState) throws {
    switch state {
    case .started, .completed:
      return
    case .errorCreatingFile:
      throw ParsingError.errorCreatingFile
    case .errorWritingHeader:
      throw ParsingError.errorWritingHeader
    case .errorWritingNodes:
      throw ParsingError.errorWritingNode
    case .cancelled:
      throw ParsingError.errorUnknown
    @unknown default:
      throw ParsingError.errorUnknown
    }
  }
}
