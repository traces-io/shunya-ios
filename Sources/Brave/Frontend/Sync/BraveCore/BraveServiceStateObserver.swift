// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation

class ShunyaServiceStateObserver: NSObject {

  // MARK: Static

  static let coreServiceLoadedNotification: Notification.Name = .init(rawValue: "ShunyaServiceStateDidLoaded")

  static var isServiceLoadStatePosted = false

  // MARK: Private

  func postServiceLoadedNotification() {
    guard !ShunyaServiceStateObserver.isServiceLoadStatePosted else {
      return
    }

    NotificationCenter.default.post(
      name: ShunyaServiceStateObserver.coreServiceLoadedNotification,
      object: nil)

    ShunyaServiceStateObserver.isServiceLoadStatePosted = true
  }
}
