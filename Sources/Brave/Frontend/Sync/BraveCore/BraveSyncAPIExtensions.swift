// Copyright 2020 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore
import Preferences
import Shared
import os.log

public struct ShunyaSyncDevice: Codable {
  let chromeVersion: String
  let hasSharingInfo: Bool
  let id: String
  let guid: String
  let isCurrentDevice: Bool
  let supportsSelfDelete: Bool
  let lastUpdatedTimestamp: TimeInterval
  let name: String?
  let os: String
  let sendTabToSelfReceivingEnabled: Bool
  let type: String
}

extension ShunyaSyncAPI {

  public static let seedByteLength = 32
  
  var isInSyncGroup: Bool {
    return Preferences.Chromium.syncEnabled.value
  }
  
  /// Property that determines if the local sync chain should be resetted
  var shouldLeaveSyncGroup: Bool {
    guard isInSyncGroup else {
      return false
    }
    
    return (!isSyncFeatureActive && !isInitialSyncFeatureSetupComplete) || isSyncAccountDeletedNoticePending
  }

  var isSendTabToSelfVisible: Bool {
    guard let json = getDeviceListJSON(), let data = json.data(using: .utf8) else {
      return false
    }
    
    do {
      let devices = try JSONDecoder().decode([ShunyaSyncDevice].self, from: data)
      return devices.count > 1
    } catch {
      Logger.module.error("Error occurred while parsing device information: \(error.localizedDescription)")
      return false
    }
  }
  
  @discardableResult
  func joinSyncGroup(codeWords: String, syncProfileService: ShunyaSyncProfileServiceIOS, shouldEnableBookmarks: Bool) -> Bool {
    if setSyncCode(codeWords) {
      enableSyncTypes(syncProfileService: syncProfileService, shouldEnableBookmarks: shouldEnableBookmarks)
      requestSync()
      setSetupComplete()
      Preferences.Chromium.syncEnabled.value = true

      return true
    }
    return false
  }

  func removeDeviceFromSyncGroup(deviceGuid: String) {
    deleteDevice(deviceGuid)
  }

  /// Method for leaving sync chain
  /// Removing Observers, clearing local preferences and calling reset chain on shunya-core side
  /// - Parameter preservingObservers: Parameter that decides if observers should be preserved or removed
  func leaveSyncGroup(preservingObservers: Bool = false) {
    if !preservingObservers {
      // Remove all observers before leaving the sync chain
      removeAllObservers()
    }
    
    resetSyncChain()
    Preferences.Chromium.syncEnabled.value = false
  }
  
  func resetSyncChain() {
    Preferences.Chromium.syncHistoryEnabled.value = false
    Preferences.Chromium.syncPasswordsEnabled.value = false
    Preferences.Chromium.syncOpenTabsEnabled.value = false
    
    resetSync()
  }

  func enableSyncTypes(syncProfileService: ShunyaSyncProfileServiceIOS, shouldEnableBookmarks: Bool) {
    syncProfileService.userSelectedTypes = []
    
    // This value is true by default
    // In some cases while joining a sync chain all values must be disabled
    Preferences.Chromium.syncBookmarksEnabled.value = shouldEnableBookmarks
      
    if Preferences.Chromium.syncBookmarksEnabled.value {
      syncProfileService.userSelectedTypes.update(with: .BOOKMARKS)
    }

    if Preferences.Chromium.syncHistoryEnabled.value {
      syncProfileService.userSelectedTypes.update(with: .HISTORY)
    }

    if Preferences.Chromium.syncPasswordsEnabled.value {
      syncProfileService.userSelectedTypes.update(with: .PASSWORDS)
    }
    
    if Preferences.Chromium.syncOpenTabsEnabled.value {
      syncProfileService.userSelectedTypes.update(with: .TABS)
    }
  }

  /// Method to add observer for SyncService for onStateChanged and onSyncShutdown
  /// OnStateChanged can be called in various situations like successful initialization - services unavaiable
  /// sync shutdown - sync errors - sync chain deleted
  /// - Parameters:
  ///   - onStateChanged: Callback for sync service state changes
  ///   - onServiceShutdown: Callback for sync service shutdown
  /// - Returns: Listener for service
  func addServiceStateObserver(_ onStateChanged: @escaping () -> Void, onServiceShutdown: @escaping () -> Void = {}) -> AnyObject {
    let serviceStateListener = ShunyaSyncServiceListener(onRemoved: { [weak self] observer in
      self?.serviceObservers.remove(observer)
    })
    serviceStateListener.observer = createSyncServiceObserver(onStateChanged, onSyncServiceShutdown: onServiceShutdown)

    serviceObservers.add(serviceStateListener)
    return serviceStateListener
  }

  func addDeviceStateObserver(_ observer: @escaping () -> Void) -> AnyObject {
    let deviceStateListener = ShunyaSyncDeviceListener(
      observer,
      onRemoved: { [weak self] observer in
        self?.deviceObservers.remove(observer)
      })
    deviceStateListener.observer = createSyncDeviceObserver(observer)

    deviceObservers.add(deviceStateListener)
    return deviceStateListener
  }

  public func removeAllObservers() {
    serviceObservers.objectEnumerator().forEach({
      ($0 as? ShunyaSyncServiceListener)?.observer = nil
    })

    deviceObservers.objectEnumerator().forEach({
      ($0 as? ShunyaSyncDeviceListener)?.observer = nil
    })

    serviceObservers.removeAllObjects()
    deviceObservers.removeAllObjects()
  }

  private struct AssociatedObjectKeys {
    static var serviceObservers: Int = 0
    static var deviceObservers: Int = 1
  }

  private var serviceObservers: NSHashTable<ShunyaSyncServiceListener> {
    if let observers = objc_getAssociatedObject(self, &AssociatedObjectKeys.serviceObservers) as? NSHashTable<ShunyaSyncServiceListener> {
      return observers
    }

    let defaultValue = NSHashTable<ShunyaSyncServiceListener>.weakObjects()
    objc_setAssociatedObject(self, &AssociatedObjectKeys.serviceObservers, defaultValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)

    return defaultValue
  }

  private var deviceObservers: NSHashTable<ShunyaSyncDeviceListener> {
    if let observers = objc_getAssociatedObject(self, &AssociatedObjectKeys.deviceObservers) as? NSHashTable<ShunyaSyncDeviceListener> {
      return observers
    }

    let defaultValue = NSHashTable<ShunyaSyncDeviceListener>.weakObjects()
    objc_setAssociatedObject(self, &AssociatedObjectKeys.deviceObservers, defaultValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)

    return defaultValue
  }

}

extension ShunyaSyncAPI {
  private class ShunyaSyncServiceListener: NSObject {

    // MARK: Internal

    var observer: Any?
    private var onRemoved: (ShunyaSyncServiceListener) -> Void

    // MARK: Lifecycle

    fileprivate init(onRemoved: @escaping (ShunyaSyncServiceListener) -> Void) {
      self.onRemoved = onRemoved
      super.init()
    }

    deinit {
      self.onRemoved(self)
    }
  }

  private class ShunyaSyncDeviceListener: NSObject {

    // MARK: Internal

    var observer: Any?
    private var onRemoved: (ShunyaSyncDeviceListener) -> Void

    // MARK: Lifecycle

    fileprivate init(
      _ onDeviceInfoChanged: @escaping () -> Void,
      onRemoved: @escaping (ShunyaSyncDeviceListener) -> Void
    ) {
      self.onRemoved = onRemoved
      super.init()
    }

    deinit {
      self.onRemoved(self)
    }
  }
}

extension ShunyaSyncAPI.QrCodeDataValidationResult {
  var errorDescription: String {
    switch self {
    case .valid:
      return ""
    case .notWellFormed:
      return Strings.invalidSyncCodeDescription
    case .versionDeprecated:
      return Strings.syncDeprecatedVersionError
    case .expired:
      return Strings.syncExpiredError
    case .validForTooLong:
      return Strings.syncValidForTooLongError
    default:
      assertionFailure("Invalid Error Description")
      return Strings.invalidSyncCodeDescription
    }
  }
}

extension ShunyaSyncAPI.WordsValidationStatus {
  var errorDescription: String {
    switch self {
    case .valid:
      return ""
    case .notValidPureWords:
      return Strings.invalidSyncCodeDescription
    case .versionDeprecated:
      return Strings.syncDeprecatedVersionError
    case .expired:
      return Strings.syncExpiredError
    case .validForTooLong:
      return Strings.syncValidForTooLongError
    case .wrongWordsNumber:
      return Strings.notEnoughWordsDescription
    default:
      assertionFailure("Invalid Error Description")
      return Strings.invalidSyncCodeDescription
    }
  }
}
