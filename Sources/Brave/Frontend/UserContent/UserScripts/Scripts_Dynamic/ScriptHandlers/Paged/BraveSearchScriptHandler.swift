// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import WebKit
import Shared
import Preferences
import ShunyaCore
import os.log

class ShunyaSearchScriptHandler: TabContentScript {
  private weak var tab: Tab?
  private let profile: Profile
  private weak var rewards: ShunyaRewards?

  /// Tracks how many in current browsing session the user has been prompted to set Shunya Search as a default
  /// while on one of Shunya Search websites.
  private static var canSetAsDefaultCounter = 0
  /// How many times user should be shown the default browser prompt on Shunya Search websites.
  private let maxCountOfDefaultBrowserPromptsPerSession = 3
  /// How many times user is shown the default browser prompt in total, this does not reset between app launches.
  private let maxCountOfDefaultBrowserPromptsTotal = 10

  required init(tab: Tab, profile: Profile, rewards: ShunyaRewards) {
    self.tab = tab
    self.profile = profile
    self.rewards = rewards
  }
  
  static let scriptName = "ShunyaSearchScript"
  static let scriptId = UUID().uuidString
  static let messageHandlerName = "\(scriptName)_\(messageUUID)"
  static let scriptSandbox: WKContentWorld = .page
  static let userScript: WKUserScript? = {
    guard var script = loadUserScript(named: scriptName) else {
      return nil
    }
    return WKUserScript(source: secureScript(handlerName: messageHandlerName,
                                             securityToken: scriptId,
                                             script: script),
                        injectionTime: .atDocumentStart,
                        forMainFrameOnly: false,
                        in: scriptSandbox)
  }()

  private enum Method: Int {
    case canSetShunyaSearchAsDefault = 1
    case setShunyaSearchDefault = 2
  }

  private struct MethodModel: Codable {
    enum CodingKeys: String, CodingKey {
      case methodId = "method_id"
    }

    let methodId: Int
  }

  func userContentController(
    _ userContentController: WKUserContentController,
    didReceiveScriptMessage message: WKScriptMessage,
    replyHandler: (Any?, String?) -> Void
  ) {
    if !verifyMessage(message: message) {
      assertionFailure("Missing required security token.")
      return
    }
    
    let allowedHosts = DomainUserScript.shunyaSearchHelper.associatedDomains

    guard let requestHost = message.frameInfo.request.url?.host,
      allowedHosts.contains(requestHost),
      message.frameInfo.isMainFrame
    else {
      Logger.module.error("Backup search request called from disallowed host")
      replyHandler(nil, nil)
      return
    }

    guard let data = try? JSONSerialization.data(withJSONObject: message.body, options: []),
      let method = try? JSONDecoder().decode(MethodModel.self, from: data).methodId
    else {
      Logger.module.error("Failed to retrieve method id")
      replyHandler(nil, nil)
      return
    }

    switch method {
    case Method.canSetShunyaSearchAsDefault.rawValue:
      handleCanSetShunyaSearchAsDefault(replyHandler: replyHandler)
    case Method.setShunyaSearchDefault.rawValue:
      handleSetShunyaSearchDefault(replyHandler: replyHandler)
    default:
      break
    }
  }

  private func handleCanSetShunyaSearchAsDefault(replyHandler: (Any?, String?) -> Void) {
    if tab?.isPrivate == true {
      Logger.module.debug("Private mode detected, skipping setting Shunya Search as a default")
      replyHandler(false, nil)
      return
    }

    let maximumPromptCount = Preferences.Search.shunyaSearchDefaultBrowserPromptCount
    if Self.canSetAsDefaultCounter >= maxCountOfDefaultBrowserPromptsPerSession || maximumPromptCount.value >= maxCountOfDefaultBrowserPromptsTotal {
      Logger.module.debug("Maximum number of tries of Shunya Search website prompts reached")
      replyHandler(false, nil)
      return
    }

    Self.canSetAsDefaultCounter += 1
    maximumPromptCount.value += 1

    let defaultEngine = profile.searchEngines.defaultEngine(forType: .standard).shortName
    let canSetAsDefault = defaultEngine != OpenSearchEngine.EngineNames.shunya
    replyHandler(canSetAsDefault, nil)
  }

  private func handleSetShunyaSearchDefault(replyHandler: (Any?, String?) -> Void) {
    profile.searchEngines.updateDefaultEngine(OpenSearchEngine.EngineNames.shunya, forType: .standard)
    replyHandler(nil, nil)
  }
}
