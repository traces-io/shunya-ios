// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

'use strict';

window.__firefox__.includeOnce("ShunyaSearchScript", function($) {
  let sendMessage = $(function(method_id) {
    return $.postNativeMessage('$<message_handler>', { 'securityToken': SECURITY_TOKEN, 'method_id': method_id});
  });
  
  Object.defineProperty(window, 'shunya', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: {
      getCanSetDefaultSearchProvider() {
        return sendMessage(1);
      },
      
      setIsDefaultSearchProvider() {
        return sendMessage(2);
      }
    }
  });
});
