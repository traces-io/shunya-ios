window.__firefox__.includeOnce("PlaylistFolderSharingScript", function($) {
  let sendMessage = $(function(pageUrl) {
    $.postNativeMessage('$<message_handler>', {
      "securityToken": SECURITY_TOKEN,
      "pageUrl": pageUrl
    });
  });
  
  if (!window.shunya) {
    window.shunya = {};
  }
  
  if (!window.shunya.playlist) {
    window.shunya.playlist = {};
    window.shunya.playlist.open = $(function(pageUrl) {
      sendMessage(pageUrl);
    });
  }
});
