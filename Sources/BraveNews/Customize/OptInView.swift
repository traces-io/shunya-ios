// Copyright 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import SwiftUI
import ShunyaUI
import DesignSystem
import ShunyaStrings
import Lottie

struct OptInView: View {
  var tappedTurnOn: @Sendable () async throws -> Void
  var tappedLearnMore: () -> Void
  
  @State private var isLoading: Bool = false
  
  var body: some View {
    VStack(spacing: 36) {
      Image("opt-in-news-graphic", bundle: .module)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .padding(.horizontal)
      VStack(spacing: 12) {
        Text(Strings.ShunyaNews.introCardTitle)
          .font(.title3.bold())
          .multilineTextAlignment(.center)
          .foregroundColor(Color(.shunyaPrimary))
        Text(Strings.ShunyaNews.introCardBody)
          .font(.subheadline)
          .multilineTextAlignment(.center)
          .foregroundColor(Color(.shunyaLabel))
      }
      VStack(spacing: 16) {
        Button(action: {
          isLoading = true
          Task {
            try await tappedTurnOn()
            isLoading = false
          }
        }) {
          Text(Strings.ShunyaNews.turnOnShunyaNews)
            .opacity(isLoading ? 0 : 1)
            .overlay(
              ProgressView()
                .progressViewStyle(.shunyaCircular(size: .small, tint: .white))
                .opacity(isLoading ? 1 : 0)
            )
            .animation(.default, value: isLoading)
        }
        .buttonStyle(ShunyaFilledButtonStyle(size: .large))
        Button(action: tappedLearnMore) {
          Text(Strings.ShunyaNews.learnMoreTitle)
            .font(.subheadline.weight(.semibold))
            .foregroundColor(Color(.shunyaBlurpleTint))
        }
      }
    }
    .padding()
    .accessibilityEmbedInScrollView()
  }
}

#if DEBUG
struct OptInView_PreviewProvider: PreviewProvider {
  static var previews: some View {
    OptInView(
      tappedTurnOn: {
        try await Task.sleep(nanoseconds: NSEC_PER_SEC * 2)
      },
      tappedLearnMore: { }
    )
  }
}
#endif
