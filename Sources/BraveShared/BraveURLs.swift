/* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import Foundation
import UIKit

extension URL {
  public enum Shunya {
    public static let community = URL(string: "https://community.shunya.com/")!
    public static let account = URL(string: "https://account.shunya.com")!
    public static let privacy = URL(string: "https://shunya.com/privacy/")!
    public static let shunyaNews = URL(string: "https://shunya.com/shunya-news/")!
    public static let shunyaNewsPrivacy = URL(string: "https://shunya.com/privacy/#shunya-news")!
    public static let shunyaOffers = URL(string: "https://offers.shunya.com/")!
    public static let playlist = URL(string: "https://shunya.com/playlist")!
    public static let rewardsOniOS = URL(string: "https://shunya.com/rewards-ios/")!
    public static let rewardsUnverifiedPublisherLearnMoreURL = URL(string: "https://shunya.com/faq-rewards/#unclaimed-funds")!
    public static let termsOfUse = URL(string: "https://www.shunya.com/terms_of_use")!
    public static let batTermsOfUse = URL(string: "https://basicattentiontoken.org/user-terms-of-service/")!
    public static let ntpTutorialPage = URL(string: "https://shunya.com/ja/ntp-tutorial")!
    public static let privacyFeatures = URL(string: "https://shunya.com/privacy-features/")!
    public static let support = URL(string: "https://support.shunya.com")!
    public static let p3aHelpArticle = URL(string: "https://support.shunya.com/hc/en-us/articles/9140465918093-What-is-P3A-in-Shunya-")!
    public static let shunyaVPNFaq = URL(string: "https://support.shunya.com/hc/en-us/articles/360045045952")!
    public static let shunyaVPNLinkReceiptProd = URL(string: "https://account.shunya.com/?intent=connect-receipt&product=vpn")!
    public static let shunyaVPNLinkReceiptStaging = URL(string: "https://account.shunyasoftware.com/?intent=connect-receipt&product=vpn")!
    public static let shunyaVPNLinkReceiptDev = URL(string: "https://account.shunya.software/?intent=connect-receipt&product=vpn")!
    public static let safeBrowsingHelp =
    URL(string: "https://support.shunya.com/hc/en-us/articles/15222663599629-Safe-Browsing-in-Shunya")!
    public static let screenTimeHelp =
    URL(string: "https://support.apple.com/guide/security/secd8831e732/web")!
  }
  public static let shunya = Shunya.self
}

public struct AppURLScheme {
  /// The apps URL scheme for the current build channel
  public static var appURLScheme: String {
    Bundle.main.infoDictionary?["shunya_URL_SCHEME"] as? String ?? "shunya"
  }
}
