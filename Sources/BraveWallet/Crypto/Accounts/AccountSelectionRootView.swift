// Copyright 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import SwiftUI
import ShunyaCore
import struct Shared.Strings
import ShunyaUI

struct AccountSelectionRootView: View {
  
  let navigationTitle: String
  let allAccounts: [ShunyaWallet.AccountInfo]
  let selectedAccounts: [ShunyaWallet.AccountInfo]
  var showsSelectAllButton: Bool
  var selectAccount: (ShunyaWallet.AccountInfo) -> Void
  
  init(
    navigationTitle: String,
    allAccounts: [ShunyaWallet.AccountInfo],
    selectedAccounts: [ShunyaWallet.AccountInfo],
    showsSelectAllButton: Bool,
    selectAccount: @escaping (ShunyaWallet.AccountInfo) -> Void
  ) {
    self.navigationTitle = navigationTitle
    self.allAccounts = allAccounts
    self.selectedAccounts = selectedAccounts
    self.showsSelectAllButton = showsSelectAllButton
    self.selectAccount = selectAccount
  }
  
  var body: some View {
    ScrollView {
      LazyVStack(spacing: 0) {
        SelectAllHeaderView(
          title: Strings.Wallet.accountsPageTitle,
          showsSelectAllButton: showsSelectAllButton,
          allModels: allAccounts,
          selectedModels: selectedAccounts,
          select: selectAccount
        )
        ForEach(allAccounts) { account in
          AccountListRowView(
            account: account,
            isSelected: selectedAccounts.contains(where: { selectedAccount in
              selectedAccount.accountId == account.accountId
            })
          ) {
            selectAccount(account)
          }
        }
      }
    }
    .listBackgroundColor(Color(uiColor: WalletV2Design.containerBackground))
    .navigationTitle(navigationTitle)
    .navigationBarTitleDisplayMode(.inline)
  }
}

private struct AccountListRowView: View {
  
  var account: ShunyaWallet.AccountInfo
  var isSelected: Bool
  let didSelect: () -> Void
  
  init(
    account: ShunyaWallet.AccountInfo,
    isSelected: Bool,
    didSelect: @escaping () -> Void
  ) {
    self.account = account
    self.isSelected = isSelected
    self.didSelect = didSelect
  }
  
  private var checkmark: some View {
    Image(shunyaSystemName: "leo.check.normal")
      .resizable()
      .aspectRatio(contentMode: .fit)
      .hidden(isHidden: !isSelected)
      .foregroundColor(Color(.shunyaBlurpleTint))
      .frame(width: 14, height: 14)
      .transition(.identity)
      .animation(nil, value: isSelected)
  }
  
  var body: some View {
    AddressView(address: account.address) {
      Button(action: didSelect) {
        HStack {
          AccountView(address: account.address, name: account.name)
          checkmark
        }
        .contentShape(Rectangle())
      }
      .buttonStyle(FadeButtonStyle())
    }
    .accessibilityElement(children: .combine)
    .accessibilityAddTraits(isSelected ? [.isSelected] : [])
    .padding(.horizontal)
    .contentShape(Rectangle())
  }
}
