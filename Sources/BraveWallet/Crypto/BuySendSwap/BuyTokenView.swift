// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import SwiftUI
import DesignSystem
import ShunyaCore
import Strings
import ShunyaUI

struct BuyTokenView: View {
  @ObservedObject var keyringStore: KeyringStore
  @ObservedObject var networkStore: NetworkStore
  @ObservedObject var buyTokenStore: BuyTokenStore

  @State private var isShowingProviderSelection = false
  
  var onDismiss: (() -> Void)

  var body: some View {
    NavigationView {
      Form {
        Section {
          AccountPicker(
            keyringStore: keyringStore,
            networkStore: networkStore
          )
          .listRowBackground(Color(UIColor.shunyaGroupedBackground))
          .resetListHeaderStyle()
        }
        if buyTokenStore.isSelectedNetworkSupported {
          Section(
            header: WalletListHeaderView(title: Text(Strings.Wallet.buy))
          ) {
            NavigationLink(destination: BuyTokenSearchView(buyTokenStore: buyTokenStore, network: networkStore.defaultSelectedChain)
            ) {
              HStack {
                if let token = buyTokenStore.selectedBuyToken {
                  AssetIconView(token: token, network: networkStore.defaultSelectedChain, length: 26)
                }
                Text(buyTokenStore.selectedBuyToken?.symbol ?? "BAT")
                  .font(.title3.weight(.semibold))
                  .foregroundColor(Color(.shunyaLabel))
              }
              .padding(.vertical, 8)
            }
            .listRowBackground(Color(.secondaryShunyaGroupedBackground))
          }
          Section(
            header: WalletListHeaderView(title: Text(Strings.Wallet.enterAmount))
          ) {
            HStack {
              Menu {
                Picker(
                  selection: $buyTokenStore.selectedCurrency,
                  content: {
                    ForEach(buyTokenStore.supportedCurrencies) { currency in
                      Text(currency.currencyCode)
                        .foregroundColor(Color(.secondaryShunyaLabel))
                        .tag(currency)
                    }
                  },
                  label: { EmptyView() } // `Menu` label is used instead
                )
              } label: {
                HStack(spacing: 4) {
                  Text(buyTokenStore.selectedCurrency.symbol)
                    .font(.title2.weight(.bold))
                    .foregroundColor(Color(.shunyaLabel))
                  Image(systemName: "chevron.down")
                    .imageScale(.small)
                    .foregroundColor(Color(.secondaryShunyaLabel))
                }
                .padding(.vertical, 4)
              }
              TextField("0", text: $buyTokenStore.buyAmount)
                .keyboardType(.decimalPad)
            }
            .listRowBackground(Color(.secondaryShunyaGroupedBackground))
          }
          Section(
            header: HStack {
              Button(action: {
                isShowingProviderSelection = true
              }) {
                Text(Strings.Wallet.purchaseMethodButtonTitle)
              }
              .buttonStyle(ShunyaFilledButtonStyle(size: .normal))
              .frame(maxWidth: .infinity)
            }
              .resetListHeaderStyle()
              .listRowBackground(Color(.clear))
          ) {
          }
        }
      }
      .environment(\.defaultMinListHeaderHeight, 0)
      .environment(\.defaultMinListRowHeight, 0)
      .listBackgroundColor(Color(UIColor.shunyaGroupedBackground))
      .navigationTitle(Strings.Wallet.buy)
      .navigationBarTitleDisplayMode(.inline)
      .toolbar {
        ToolbarItemGroup(placement: .cancellationAction) {
          Button(action: { onDismiss() }) {
            Text(Strings.cancelButtonTitle)
              .foregroundColor(Color(.shunyaBlurpleTint))
          }
        }
      }
      .overlay(
        Group {
          if !buyTokenStore.isSelectedNetworkSupported {
            Text(Strings.Wallet.networkNotSupportedForBuyToken)
              .font(.headline.weight(.medium))
              .frame(maxWidth: .infinity)
              .multilineTextAlignment(.center)
              .foregroundColor(Color(.secondaryShunyaLabel))
              .transition(.opacity)
          }
        }
      )
      .background(
        NavigationLink(
          isActive: $isShowingProviderSelection,
          destination: {
            BuyProviderSelectionView(
              buyTokenStore: buyTokenStore,
              keyringStore: keyringStore
            )
          },
          label: {
            EmptyView()
          })
      )
    }
    .navigationViewStyle(.stack)
  }
}

#if DEBUG
struct BuyTokenView_Previews: PreviewProvider {
  static var previews: some View {
    BuyTokenView(
      keyringStore: .previewStore,
      networkStore: .previewStore,
      buyTokenStore: .previewStore,
      onDismiss: {}
    )
    .previewColorSchemes()
  }
}
#endif
