// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore

class AccountActivityStore: ObservableObject {
  /// If we want to observe selected account changes (ex. in `WalletPanelView`).
  /// In some cases, we do not want to update the account displayed when the
  /// selected account changes (ex. when removing an account).
  let observeAccountUpdates: Bool
  private(set) var account: ShunyaWallet.AccountInfo
  @Published private(set) var userVisibleAssets: [AssetViewModel] = []
  @Published private(set) var userVisibleNFTs: [NFTAssetViewModel] = []
  @Published var transactionSummaries: [TransactionSummary] = []
  @Published private(set) var currencyCode: String = CurrencyCode.usd.code {
    didSet {
      currencyFormatter.currencyCode = currencyCode
      guard oldValue != currencyCode else { return }
      update()
    }
  }

  let currencyFormatter: NumberFormatter = .usdCurrencyFormatter

  private let keyringService: ShunyaWalletKeyringService
  private let walletService: ShunyaWalletShunyaWalletService
  private let rpcService: ShunyaWalletJsonRpcService
  private let assetRatioService: ShunyaWalletAssetRatioService
  private let txService: ShunyaWalletTxService
  private let blockchainRegistry: ShunyaWalletBlockchainRegistry
  private let solTxManagerProxy: ShunyaWalletSolanaTxManagerProxy
  private let ipfsApi: IpfsAPI
  private let assetManager: WalletUserAssetManagerType
  /// Cache for storing `BlockchainToken`s that are not in user assets or our token registry.
  /// This could occur with a dapp creating a transaction.
  private var tokenInfoCache: [String: ShunyaWallet.BlockchainToken] = [:]

  init(
    account: ShunyaWallet.AccountInfo,
    observeAccountUpdates: Bool,
    keyringService: ShunyaWalletKeyringService,
    walletService: ShunyaWalletShunyaWalletService,
    rpcService: ShunyaWalletJsonRpcService,
    assetRatioService: ShunyaWalletAssetRatioService,
    txService: ShunyaWalletTxService,
    blockchainRegistry: ShunyaWalletBlockchainRegistry,
    solTxManagerProxy: ShunyaWalletSolanaTxManagerProxy,
    ipfsApi: IpfsAPI,
    userAssetManager: WalletUserAssetManagerType
  ) {
    self.account = account
    self.observeAccountUpdates = observeAccountUpdates
    self.keyringService = keyringService
    self.walletService = walletService
    self.rpcService = rpcService
    self.assetRatioService = assetRatioService
    self.txService = txService
    self.blockchainRegistry = blockchainRegistry
    self.solTxManagerProxy = solTxManagerProxy
    self.ipfsApi = ipfsApi
    self.assetManager = userAssetManager
    
    self.keyringService.add(self)
    self.rpcService.add(self)
    self.txService.add(self)
    self.walletService.add(self)
    
    walletService.defaultBaseCurrency { [self] currencyCode in
      self.currencyCode = currencyCode
    }
  }

  func update() {
    Task { @MainActor in
      let coin = account.coin
      let networksForAccountCoin = await rpcService.allNetworks(coin)
        .filter { $0.chainId != ShunyaWallet.LocalhostChainId } // localhost not supported
      let networksForAccount = networksForAccountCoin.filter { // .fil coin type has two different keyring ids
        $0.supportedKeyrings.contains(account.keyringId.rawValue as NSNumber)
      }
      
      struct NetworkAssets: Equatable {
        let network: ShunyaWallet.NetworkInfo
        let tokens: [ShunyaWallet.BlockchainToken]
        let sortOrder: Int
      }
      let allVisibleUserAssets = assetManager.getAllVisibleAssetsInNetworkAssets(networks: networksForAccount)
      let allTokens = await blockchainRegistry.allTokens(in: networksForAccountCoin).flatMap(\.tokens)
      var updatedUserVisibleAssets: [AssetViewModel] = []
      var updatedUserVisibleNFTs: [NFTAssetViewModel] = []
      for networkAssets in allVisibleUserAssets {
        for token in networkAssets.tokens {
          if token.isErc721 || token.isNft {
            updatedUserVisibleNFTs.append(
              NFTAssetViewModel(
                token: token,
                network: networkAssets.network,
                balanceForAccounts: [:]
              )
            )
          } else {
            updatedUserVisibleAssets.append(
              AssetViewModel(
                groupType: .none,
                token: token,
                network: networkAssets.network,
                price: "",
                history: [],
                balanceForAccounts: [:]
              )
            )
          }
        }
      }
      self.userVisibleAssets = updatedUserVisibleAssets
      self.userVisibleNFTs = updatedUserVisibleNFTs
      
      let keyringForAccount = await keyringService.keyringInfo(account.keyringId)
      typealias TokenNetworkAccounts = (token: ShunyaWallet.BlockchainToken, network: ShunyaWallet.NetworkInfo, accounts: [ShunyaWallet.AccountInfo])
      let allTokenNetworkAccounts = allVisibleUserAssets.flatMap { networkAssets in
        networkAssets.tokens.map { token in
          TokenNetworkAccounts(
            token: token,
            network: networkAssets.network,
            accounts: [account]
          )
        }
      }
      let totalBalances: [String: Double] = await withTaskGroup(of: [String: Double].self, body: { @MainActor group in
        for tokenNetworkAccounts in allTokenNetworkAccounts {
          group.addTask { @MainActor in
            let totalBalance = await self.rpcService.fetchTotalBalance(
              token: tokenNetworkAccounts.token,
              network: tokenNetworkAccounts.network,
              accounts: tokenNetworkAccounts.accounts
            )
            return [tokenNetworkAccounts.token.assetBalanceId: totalBalance]
          }
        }
        return await group.reduce(into: [String: Double](), { partialResult, new in
          for key in new.keys {
            partialResult[key] = new[key]
          }
        })
      })
      
      // fetch price for every visible token
      let allVisibleTokens = allVisibleUserAssets.flatMap(\.tokens)
      let allVisibleTokenAssetRatioIds = allVisibleTokens.map(\.assetRatioId)
      let prices: [String: String] = await assetRatioService.fetchPrices(
        for: allVisibleTokenAssetRatioIds,
        toAssets: [currencyFormatter.currencyCode],
        timeframe: .oneDay
      )
      
      // fetch NFTs metadata
      let allNFTMetadata = await rpcService.fetchNFTMetadata(
        tokens: userVisibleNFTs
          .map(\.token)
          .filter({ $0.isErc721 || $0.isNft }),
        ipfsApi: ipfsApi
      )
      
      guard !Task.isCancelled else { return }
      updatedUserVisibleAssets.removeAll()
      updatedUserVisibleNFTs.removeAll()
      for networkAssets in allVisibleUserAssets {
        for token in networkAssets.tokens {
          if token.isErc721 || token.isNft {
            updatedUserVisibleNFTs.append(
              NFTAssetViewModel(
                token: token,
                network: networkAssets.network,
                balanceForAccounts: [account.address: Int(totalBalances[token.assetBalanceId] ?? 0)],
                nftMetadata: allNFTMetadata[token.id]
              )
            )
          } else {
            updatedUserVisibleAssets.append(
              AssetViewModel(
                groupType: .none,
                token: token,
                network: networkAssets.network,
                price: prices[token.assetRatioId.lowercased()] ?? "",
                history: [],
                balanceForAccounts: [account.address: totalBalances[token.assetBalanceId] ?? 0]
              )
            )
          }
        }
      }
      self.userVisibleAssets = updatedUserVisibleAssets
      self.userVisibleNFTs = updatedUserVisibleNFTs
      
      let assetRatios = self.userVisibleAssets.reduce(into: [String: Double](), {
        $0[$1.token.assetRatioId.lowercased()] = Double($1.price)
      })
      
      self.transactionSummaries = await fetchTransactionSummarys(
        networksForAccountCoin: networksForAccountCoin,
        accountInfos: keyringForAccount.accountInfos,
        userVisibleTokens: userVisibleAssets.map(\.token),
        allTokens: allTokens,
        assetRatios: assetRatios
      )
    }
  }
  
  @MainActor private func fetchTransactionSummarys(
    networksForAccountCoin: [ShunyaWallet.NetworkInfo],
    accountInfos: [ShunyaWallet.AccountInfo],
    userVisibleTokens: [ShunyaWallet.BlockchainToken],
    allTokens: [ShunyaWallet.BlockchainToken],
    assetRatios: [String: Double]
  ) async -> [TransactionSummary] {
    let transactions = await txService.allTransactions(networks: networksForAccountCoin, for: account)
    var solEstimatedTxFees: [String: UInt64] = [:]
    if account.coin == .sol {
      solEstimatedTxFees = await solTxManagerProxy.estimatedTxFees(for: transactions)
    }
    let unknownTokenContractAddresses = transactions
      .flatMap { $0.tokenContractAddresses }
      .filter { contractAddress in
        !userVisibleTokens.contains(where: { $0.contractAddress.caseInsensitiveCompare(contractAddress) == .orderedSame })
        && !allTokens.contains(where: { $0.contractAddress.caseInsensitiveCompare(contractAddress) == .orderedSame })
        && !tokenInfoCache.keys.contains(where: { $0.caseInsensitiveCompare(contractAddress) == .orderedSame })
      }
    var allTokens = allTokens
    if !unknownTokenContractAddresses.isEmpty {
      let unknownTokens = await assetRatioService.fetchTokens(for: unknownTokenContractAddresses)
      for unknownToken in unknownTokens {
        tokenInfoCache[unknownToken.contractAddress] = unknownToken
      }
      allTokens.append(contentsOf: unknownTokens)
    }
    return transactions
      .compactMap { transaction in
        guard let network = networksForAccountCoin.first(where: { $0.chainId == transaction.chainId }) else {
          return nil
        }
        return TransactionParser.transactionSummary(
          from: transaction,
          network: network,
          accountInfos: accountInfos,
          visibleTokens: userVisibleTokens,
          allTokens: allTokens,
          assetRatios: assetRatios,
          solEstimatedTxFee: solEstimatedTxFees[transaction.id],
          currencyFormatter: currencyFormatter
        )
      }.sorted(by: { $0.createdTime > $1.createdTime })
  }
  
  func transactionDetailsStore(for transaction: ShunyaWallet.TransactionInfo) -> TransactionDetailsStore {
    TransactionDetailsStore(
      transaction: transaction,
      keyringService: keyringService,
      walletService: walletService,
      rpcService: rpcService,
      assetRatioService: assetRatioService,
      blockchainRegistry: blockchainRegistry,
      solanaTxManagerProxy: solTxManagerProxy,
      userAssetManager: assetManager
    )
  }
  
  #if DEBUG
  func previewTransactions() {
    transactionSummaries = [.previewConfirmedSwap, .previewConfirmedSend, .previewConfirmedERC20Approve]
  }
  #endif
}

extension AccountActivityStore: ShunyaWalletKeyringServiceObserver {
  func keyringCreated(_ keyringId: ShunyaWallet.KeyringId) {
  }

  func keyringRestored(_ keyringId: ShunyaWallet.KeyringId) {
  }
  
  func keyringReset() {
  }
  
  func locked() {
  }
  
  func unlocked() {
  }
  
  func backedUp() {
  }
  
  func accountsChanged() {
  }
  
  func autoLockMinutesChanged() {
  }
  
  func selectedWalletAccountChanged(_ account: ShunyaWallet.AccountInfo) {
    guard observeAccountUpdates else { return }
    self.account = account
    update()
  }
  
  func selectedDappAccountChanged(_ coin: ShunyaWallet.CoinType, account: ShunyaWallet.AccountInfo?) {
    guard observeAccountUpdates, let account else { return }
    self.account = account
    update()
  }
  
  func accountsAdded(_ addedAccounts: [ShunyaWallet.AccountInfo]) {
  }
}

extension AccountActivityStore: ShunyaWalletJsonRpcServiceObserver {
  func chainChangedEvent(_ chainId: String, coin: ShunyaWallet.CoinType, origin: URLOrigin?) {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
      // Handle small gap between chain changing and txController having the correct chain Id
      self.update()
    }
  }
  func onAddEthereumChainRequestCompleted(_ chainId: String, error: String) {
  }
  func onIsEip1559Changed(_ chainId: String, isEip1559: Bool) {
  }
}

extension AccountActivityStore: ShunyaWalletTxServiceObserver {
  func onNewUnapprovedTx(_ txInfo: ShunyaWallet.TransactionInfo) {
    update()
  }
  func onTransactionStatusChanged(_ txInfo: ShunyaWallet.TransactionInfo) {
    update()
  }
  func onUnapprovedTxUpdated(_ txInfo: ShunyaWallet.TransactionInfo) {
  }
  func onTxServiceReset() {
  }
}

extension AccountActivityStore: ShunyaWalletShunyaWalletServiceObserver {
  public func onActiveOriginChanged(_ originInfo: ShunyaWallet.OriginInfo) {
  }

  public func onDefaultWalletChanged(_ wallet: ShunyaWallet.DefaultWallet) {
  }

  public func onDefaultBaseCurrencyChanged(_ currency: String) {
    currencyCode = currency
  }

  public func onDefaultBaseCryptocurrencyChanged(_ cryptocurrency: String) {
  }

  public func onNetworkListChanged() {
  }
  
  func onDefaultEthereumWalletChanged(_ wallet: ShunyaWallet.DefaultWallet) {
  }
  
  func onDefaultSolanaWalletChanged(_ wallet: ShunyaWallet.DefaultWallet) {
  }
  
  public func onDiscoverAssetsStarted() {
  }
  
  func onDiscoverAssetsCompleted(_ discoveredAssets: [ShunyaWallet.BlockchainToken]) {
  }
  
  func onResetWallet() {
  }
}
