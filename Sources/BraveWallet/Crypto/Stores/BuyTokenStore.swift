// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore
import OrderedCollections
import Combine

/// A store contains data for buying tokens
public class BuyTokenStore: ObservableObject {
  /// The current selected token to buy. Default with nil value.
  @Published var selectedBuyToken: ShunyaWallet.BlockchainToken?
  /// The supported currencies for purchasing
  @Published var supportedCurrencies: [ShunyaWallet.OnRampCurrency] = []
  /// A boolean indicates if the current selected network supports `Buy`
  @Published var isSelectedNetworkSupported: Bool = false
  /// The amount user wishes to purchase
  @Published var buyAmount: String = ""
  /// The currency user wishes to purchase with
  @Published var selectedCurrency: ShunyaWallet.OnRampCurrency = .init()
  
  /// A map of list of available tokens to a certain on ramp provider
  var buyTokens: [ShunyaWallet.OnRampProvider: [ShunyaWallet.BlockchainToken]]
  /// A list of all available tokens for all providers
  var allTokens: [ShunyaWallet.BlockchainToken] = []
  
  /// The supported `OnRampProvider`s for the currently selected currency and device locale.
  var supportedProviders: OrderedSet<ShunyaWallet.OnRampProvider> {
    return OrderedSet(orderedSupportedBuyOptions
      .filter { provider in
        guard let tokens = buyTokens[provider],
              let selectedBuyToken = selectedBuyToken
        else { return false }
        // verify selected currency code is supported for this provider
        guard supportedCurrencies.contains(where: { supportedOnRampCurrency in
          guard supportedOnRampCurrency.providers.contains(.init(integerLiteral: provider.rawValue)) else {
            return false
          }
          let selectedCurrencyCode = selectedCurrency.currencyCode
          return supportedOnRampCurrency.currencyCode.caseInsensitiveCompare(selectedCurrencyCode) == .orderedSame
        }) else {
          return false
        }
        // verify selected token is supported for this provider
        return tokens.includes(selectedBuyToken)
      })
  }

  private let blockchainRegistry: ShunyaWalletBlockchainRegistry
  private let keyringService: ShunyaWalletKeyringService
  private let rpcService: ShunyaWalletJsonRpcService
  private let walletService: ShunyaWalletShunyaWalletService
  private let assetRatioService: ShunyaWalletAssetRatioService
  private var selectedNetwork: ShunyaWallet.NetworkInfo = .init()
  private(set) var orderedSupportedBuyOptions: OrderedSet<ShunyaWallet.OnRampProvider> = []
  private var prefilledToken: ShunyaWallet.BlockchainToken?
  
  /// A map between chain id and gas token's symbol
  static let gasTokens: [String: [String]] = [
    ShunyaWallet.MainnetChainId: ["eth"],
    ShunyaWallet.OptimismMainnetChainId: ["eth"],
    ShunyaWallet.AuroraMainnetChainId: ["eth"],
    ShunyaWallet.PolygonMainnetChainId: ["matic"],
    ShunyaWallet.FantomMainnetChainId: ["ftm"],
    ShunyaWallet.CeloMainnetChainId: ["celo"],
    ShunyaWallet.BinanceSmartChainMainnetChainId: ["bnb"],
    ShunyaWallet.SolanaMainnet: ["sol"],
    ShunyaWallet.FilecoinMainnet: ["fil"],
    ShunyaWallet.AvalancheMainnetChainId: ["avax", "avaxc"]
  ]

  public init(
    blockchainRegistry: ShunyaWalletBlockchainRegistry,
    keyringService: ShunyaWalletKeyringService,
    rpcService: ShunyaWalletJsonRpcService,
    walletService: ShunyaWalletShunyaWalletService,
    assetRatioService: ShunyaWalletAssetRatioService,
    prefilledToken: ShunyaWallet.BlockchainToken?
  ) {
    self.blockchainRegistry = blockchainRegistry
    self.keyringService = keyringService
    self.rpcService = rpcService
    self.walletService = walletService
    self.assetRatioService = assetRatioService
    self.prefilledToken = prefilledToken
    self.buyTokens = WalletConstants.supportedOnRampProviders.reduce(
      into: [ShunyaWallet.OnRampProvider: [ShunyaWallet.BlockchainToken]]()
    ) {
      $0[$1] = []
    }
    
    self.rpcService.add(self)
    self.keyringService.add(self)
    
    Task {
      await updateInfo()
    }
  }
  
  @MainActor private func validatePrefilledToken(on network: ShunyaWallet.NetworkInfo) async {
    guard let prefilledToken = self.prefilledToken else {
      return
    }
    if prefilledToken.coin == network.coin && prefilledToken.chainId == network.chainId {
      // valid for current network
      self.selectedBuyToken = prefilledToken
    } else {
      // need to try and select correct network.
      let allNetworksForTokenCoin = await rpcService.allNetworks(prefilledToken.coin)
      guard let networkForToken = allNetworksForTokenCoin.first(where: { $0.chainId == prefilledToken.chainId }) else {
        // don't set prefilled token if it belongs to a network we don't know
        return
      }
      let success = await rpcService.setNetwork(networkForToken.chainId, coin: networkForToken.coin, origin: nil)
      if success {
        self.selectedNetwork = networkForToken
        self.selectedBuyToken = prefilledToken
      }
    }
    self.prefilledToken = nil
  }

  @MainActor
  func fetchBuyUrl(
    provider: ShunyaWallet.OnRampProvider,
    account: ShunyaWallet.AccountInfo
  ) async -> URL? {
    guard let token = selectedBuyToken else { return nil }
    
    let symbol: String
    let currencyCode: String
    switch provider {
    case .ramp:
      symbol = token.rampNetworkSymbol
      currencyCode = selectedCurrency.currencyCode
    case .stripe:
      symbol = token.symbol.lowercased()
      currencyCode = selectedCurrency.currencyCode.lowercased()
    default:
      symbol = token.symbol
      currencyCode = selectedCurrency.currencyCode
    }
    
    let (urlString, error) = await assetRatioService.buyUrlV1(
      provider,
      chainId: selectedNetwork.chainId,
      address: account.address,
      symbol: symbol,
      amount: buyAmount,
      currencyCode: currencyCode
    )

    guard error == nil, let url = URL(string: urlString) else {
      return nil
    }
    
    return url
  }

  @MainActor
  private func fetchBuyTokens(network: ShunyaWallet.NetworkInfo) async {
    allTokens = []
    for provider in buyTokens.keys {
      let tokens = await blockchainRegistry.buyTokens(provider, chainId: network.chainId)
      let sortedTokenList = tokens.sorted(by: {
        if $0.isGasToken, !$1.isGasToken {
          return true
        } else if !$0.isGasToken, $1.isGasToken {
          return false
        } else if $0.isBatToken, !$1.isBatToken {
          return true
        } else if !$0.isBatToken, $1.isBatToken {
          return false
        } else {
          return $0.symbol < $1.symbol
        }
      })
      buyTokens[provider] = sortedTokenList
    }
    
    for provider in orderedSupportedBuyOptions {
      if let tokens = buyTokens[provider] {
        for token in tokens where !allTokens.includes(token) {
          allTokens.append(token)
        }
      }
    }
    
    if selectedBuyToken == nil || selectedBuyToken?.chainId != network.chainId {
      selectedBuyToken = allTokens.first
    }
  }
  
  @MainActor
  func updateInfo() async {
    orderedSupportedBuyOptions = ShunyaWallet.OnRampProvider.allSupportedOnRampProviders
    
    guard let selectedAccount = await keyringService.allAccounts().selectedAccount else {
      assertionFailure("selectedAccount should never be nil.")
      return
    }
    selectedNetwork = await rpcService.network(selectedAccount.coin, origin: nil)
    await validatePrefilledToken(on: selectedNetwork) // selectedNetwork may change
    await fetchBuyTokens(network: selectedNetwork)
    
    // check if current selected network supports buy
    if WalletConstants.supportedTestNetworkChainIds.contains(selectedNetwork.chainId) {
      isSelectedNetworkSupported = false
    } else {
      isSelectedNetworkSupported = allTokens.contains(where: { token in
        return token.chainId.caseInsensitiveCompare(selectedNetwork.chainId) == .orderedSame
      })
    }
    
    // fetch all available currencies for on ramp providers
    supportedCurrencies = await blockchainRegistry.onRampCurrencies()
    if let usdCurrency = supportedCurrencies.first(where: {
      $0.currencyCode.caseInsensitiveCompare(CurrencyCode.usd.code) == .orderedSame
    }) {
      selectedCurrency = usdCurrency
    } else if let firstCurrency = supportedCurrencies.first {
      selectedCurrency = firstCurrency
    }
  }
}

extension BuyTokenStore: ShunyaWalletJsonRpcServiceObserver {
  public func chainChangedEvent(_ chainId: String, coin: ShunyaWallet.CoinType, origin: URLOrigin?) {
    Task {
      await updateInfo()
    }
  }
  
  public func onAddEthereumChainRequestCompleted(_ chainId: String, error: String) {
  }
  
  public func onIsEip1559Changed(_ chainId: String, isEip1559: Bool) {
  }
}

extension BuyTokenStore: ShunyaWalletKeyringServiceObserver {
  public func keyringCreated(_ keyringId: ShunyaWallet.KeyringId) {
  }
  
  public func keyringRestored(_ keyringId: ShunyaWallet.KeyringId) {
  }
  
  public func keyringReset() {
  }
  
  public func locked() {
  }
  
  public func unlocked() {
  }
  
  public func backedUp() {
  }
  
  public func accountsChanged() {
  }
  
  public func accountsAdded(_ addedAccounts: [ShunyaWallet.AccountInfo]) {
  }
  
  public func autoLockMinutesChanged() {
  }
  
  public func selectedWalletAccountChanged(_ account: ShunyaWallet.AccountInfo) {
    Task { @MainActor in
      await updateInfo()
    }
  }
  
  public func selectedDappAccountChanged(_ coin: ShunyaWallet.CoinType, account: ShunyaWallet.AccountInfo?) {
  }
}

private extension ShunyaWallet.BlockchainToken {
  var isGasToken: Bool {
    guard let gasTokensByChain = BuyTokenStore.gasTokens[chainId] else { return false }
    return gasTokensByChain.contains { $0.caseInsensitiveCompare(symbol) == .orderedSame }
  }
  
  var isBatToken: Bool {
    // BAT/wormhole BAT/Avalanche C-Chain BAT
    return symbol.caseInsensitiveCompare("bat") == .orderedSame || symbol.caseInsensitiveCompare("wbat") == .orderedSame || symbol.caseInsensitiveCompare("bat.e") == .orderedSame
  }
  
  // a special symbol to fetch correct ramp.network buy url
  var rampNetworkSymbol: String {
    if symbol.caseInsensitiveCompare("bat") == .orderedSame && chainId.caseInsensitiveCompare(ShunyaWallet.MainnetChainId) == .orderedSame {
      // BAT is the only token on Ethereum Mainnet with a prefix on Ramp.Network
      return "ETH_BAT"
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.AvalancheMainnetChainId) == .orderedSame && contractAddress.isEmpty {
      // AVAX native token has no prefix
      return symbol
    } else {
      let rampNetworkPrefix: String
      switch chainId.lowercased() {
      case ShunyaWallet.MainnetChainId.lowercased(),
        ShunyaWallet.CeloMainnetChainId.lowercased():
        rampNetworkPrefix = ""
      case ShunyaWallet.AvalancheMainnetChainId.lowercased():
        rampNetworkPrefix = "AVAXC"
      case ShunyaWallet.BinanceSmartChainMainnetChainId.lowercased():
        rampNetworkPrefix = "BSC"
      case ShunyaWallet.PolygonMainnetChainId.lowercased():
        rampNetworkPrefix = "MATIC"
      case ShunyaWallet.SolanaMainnet.lowercased():
        rampNetworkPrefix = "SOLANA"
      case ShunyaWallet.OptimismMainnetChainId.lowercased():
        rampNetworkPrefix = "OPTIMISM"
      case ShunyaWallet.FilecoinMainnet.lowercased():
        rampNetworkPrefix = "FILECOIN"
      default:
        rampNetworkPrefix = ""
      }
      
      return rampNetworkPrefix.isEmpty ? symbol : "\(rampNetworkPrefix)_\(symbol.uppercased())"
    }
  }
}
