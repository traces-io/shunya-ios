// Copyright 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import ShunyaCore

struct CoinViewModel: Identifiable {
  var id: String
  var name: String
  var symbol: String
  var rank: Int
  var price: String
  var priceChangePercentage24h: String
}

public class MarketStore: ObservableObject {
  /// Avalaible coins in market
  @Published var coins: [ShunyaWallet.CoinMarket] = []
  /// Currency code for prices
  @Published private(set) var currencyCode: String = CurrencyCode.usd.code {
    didSet {
      priceFormatter.currencyCode = currencyCode
      guard oldValue != currencyCode else { return }
      update()
    }
  }
  /// Indicates `MarketStore` is loading coins in markets
  @Published var isLoading: Bool = false
  
  private let assetRatioService: ShunyaWalletAssetRatioService
  private let blockchainRegistry: ShunyaWalletBlockchainRegistry
  private let rpcService: ShunyaWalletJsonRpcService
  private let walletService: ShunyaWalletShunyaWalletService
  private let assetsRequestLimit = 250
  let priceFormatter: NumberFormatter = .usdCurrencyFormatter
  let priceChangeFormatter = NumberFormatter().then {
    $0.numberStyle = .percent
    $0.minimumFractionDigits = 2
    $0.roundingMode = .up
  }
  
  init(
    assetRatioService: ShunyaWalletAssetRatioService,
    blockchainRegistry: ShunyaWalletBlockchainRegistry,
    rpcService: ShunyaWalletJsonRpcService,
    walletService: ShunyaWalletShunyaWalletService
  ) {
    self.assetRatioService = assetRatioService
    self.blockchainRegistry = blockchainRegistry
    self.rpcService = rpcService
    self.walletService = walletService
  }
  
  private var updateTask: Task<Void, Never>?
  func update() {
    isLoading = true
    updateTask?.cancel()
    updateTask = Task { @MainActor in
      // update market coins
      guard !Task.isCancelled else { return }
      let (success, assets) = await assetRatioService.coinMarkets(priceFormatter.currencyCode, limit: UInt8(assetsRequestLimit))
      if success {
        self.coins = assets
      }
      // update currency code
      guard !Task.isCancelled else { return }
      self.currencyCode = await walletService.defaultBaseCurrency()
      
      self.isLoading = false
    }
  }
}
