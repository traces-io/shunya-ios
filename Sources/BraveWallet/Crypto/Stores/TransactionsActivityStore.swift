/* Copyright 2023 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import ShunyaCore
import SwiftUI

class TransactionsActivityStore: ObservableObject {
  @Published var transactionSummaries: [TransactionSummary] = []
  
  @Published private(set) var currencyCode: String = CurrencyCode.usd.code {
    didSet {
      currencyFormatter.currencyCode = currencyCode
      guard oldValue != currencyCode else { return }
      update()
    }
  }
  @Published var networkFilters: [Selectable<ShunyaWallet.NetworkInfo>] = [] {
    didSet {
      guard !oldValue.isEmpty else { return } // initial assignment to `networkFilters`
      update()
    }
  }
  
  let currencyFormatter: NumberFormatter = .usdCurrencyFormatter
  
  private var solEstimatedTxFeesCache: [String: UInt64] = [:]
  private var assetPricesCache: [String: Double] = [:]
  
  private let keyringService: ShunyaWalletKeyringService
  private let rpcService: ShunyaWalletJsonRpcService
  private let walletService: ShunyaWalletShunyaWalletService
  private let assetRatioService: ShunyaWalletAssetRatioService
  private let blockchainRegistry: ShunyaWalletBlockchainRegistry
  private let txService: ShunyaWalletTxService
  private let solTxManagerProxy: ShunyaWalletSolanaTxManagerProxy
  private let assetManager: WalletUserAssetManagerType
  
  init(
    keyringService: ShunyaWalletKeyringService,
    rpcService: ShunyaWalletJsonRpcService,
    walletService: ShunyaWalletShunyaWalletService,
    assetRatioService: ShunyaWalletAssetRatioService,
    blockchainRegistry: ShunyaWalletBlockchainRegistry,
    txService: ShunyaWalletTxService,
    solTxManagerProxy: ShunyaWalletSolanaTxManagerProxy,
    userAssetManager: WalletUserAssetManagerType
  ) {
    self.keyringService = keyringService
    self.rpcService = rpcService
    self.walletService = walletService
    self.assetRatioService = assetRatioService
    self.blockchainRegistry = blockchainRegistry
    self.txService = txService
    self.solTxManagerProxy = solTxManagerProxy
    self.assetManager = userAssetManager
    
    keyringService.add(self)
    txService.add(self)
    walletService.add(self)

    Task { @MainActor in
      self.currencyCode = await walletService.defaultBaseCurrency()
    }
  }
  
  private var updateTask: Task<Void, Never>?
  func update() {
    updateTask?.cancel()
    updateTask = Task { @MainActor in
      let allKeyrings = await self.keyringService.keyrings(
        for: WalletConstants.supportedCoinTypes()
      )
      let allAccountInfos = allKeyrings.flatMap(\.accountInfos)
      // setup network filters if not currently setup
      if self.networkFilters.isEmpty {
        self.networkFilters = await self.rpcService.allNetworksForSupportedCoins().map {
          .init(isSelected: true, model: $0)
        }
      }
      let networks = networkFilters.filter(\.isSelected).map(\.model)
      let networksForCoin: [ShunyaWallet.CoinType: [ShunyaWallet.NetworkInfo]] = Dictionary(grouping: networks, by: \.coin)
      let allNetworksAllCoins = networksForCoin.values.flatMap { $0 }
      
      let allTransactions = await txService.allTransactions(
        networksForCoin: networksForCoin, for: allKeyrings
      ).filter { $0.txStatus != .rejected }
      let userVisibleTokens = assetManager.getAllVisibleAssetsInNetworkAssets(networks: allNetworksAllCoins).flatMap(\.tokens)
      let allTokens = await blockchainRegistry.allTokens(
        in: allNetworksAllCoins
      ).flatMap(\.tokens)
      guard !Task.isCancelled else { return }
      // display transactions prior to network request to fetch
      // estimated solana tx fees & asset prices
      self.transactionSummaries = self.transactionSummaries(
        transactions: allTransactions,
        networksForCoin: networksForCoin,
        accountInfos: allAccountInfos,
        userVisibleTokens: userVisibleTokens,
        allTokens: allTokens,
        assetRatios: assetPricesCache,
        solEstimatedTxFees: solEstimatedTxFeesCache
      )
      guard !self.transactionSummaries.isEmpty else { return }

      if allTransactions.contains(where: { $0.coin == .sol }) {
        let solTransactions = allTransactions.filter { $0.coin == .sol }
        await updateSolEstimatedTxFeesCache(solTransactions)
      }

      let allVisibleTokenAssetRatioIds = userVisibleTokens.map(\.assetRatioId)
      await updateAssetPricesCache(assetRatioIds: allVisibleTokenAssetRatioIds)

      guard !Task.isCancelled else { return }
      self.transactionSummaries = self.transactionSummaries(
        transactions: allTransactions,
        networksForCoin: networksForCoin,
        accountInfos: allAccountInfos,
        userVisibleTokens: userVisibleTokens,
        allTokens: allTokens,
        assetRatios: assetPricesCache,
        solEstimatedTxFees: solEstimatedTxFeesCache
      )
    }
  }
  
  private func transactionSummaries(
    transactions: [ShunyaWallet.TransactionInfo],
    networksForCoin: [ShunyaWallet.CoinType: [ShunyaWallet.NetworkInfo]],
    accountInfos: [ShunyaWallet.AccountInfo],
    userVisibleTokens: [ShunyaWallet.BlockchainToken],
    allTokens: [ShunyaWallet.BlockchainToken],
    assetRatios: [String: Double],
    solEstimatedTxFees: [String: UInt64]
  ) -> [TransactionSummary] {
    transactions.compactMap { transaction in
      guard let networks = networksForCoin[transaction.coin], let network = networks.first(where: { $0.chainId == transaction.chainId }) else {
        return nil
      }
      return TransactionParser.transactionSummary(
        from: transaction,
        network: network,
        accountInfos: accountInfos,
        visibleTokens: userVisibleTokens,
        allTokens: allTokens,
        assetRatios: assetRatios,
        solEstimatedTxFee: solEstimatedTxFees[transaction.id],
        currencyFormatter: currencyFormatter
      )
    }.sorted(by: { $0.createdTime > $1.createdTime })
  }
  
  @MainActor private func updateSolEstimatedTxFeesCache(_ solTransactions: [ShunyaWallet.TransactionInfo]) async {
    let fees = await solTxManagerProxy.estimatedTxFees(for: solTransactions)
    for (key, value) in fees { // update cached values
      self.solEstimatedTxFeesCache[key] = value
    }
  }
  
  @MainActor private func updateAssetPricesCache(assetRatioIds: [String]) async {
    let prices = await assetRatioService.fetchPrices(
      for: assetRatioIds,
      toAssets: [currencyFormatter.currencyCode],
      timeframe: .oneDay
    ).compactMapValues { Double($0) }
    for (key, value) in prices { // update cached values
      self.assetPricesCache[key] = value
    }
  }
  
  func transactionDetailsStore(
    for transaction: ShunyaWallet.TransactionInfo
  ) -> TransactionDetailsStore {
    TransactionDetailsStore(
      transaction: transaction,
      keyringService: keyringService,
      walletService: walletService,
      rpcService: rpcService,
      assetRatioService: assetRatioService,
      blockchainRegistry: blockchainRegistry,
      solanaTxManagerProxy: solTxManagerProxy,
      userAssetManager: assetManager
    )
  }
}

extension TransactionsActivityStore: ShunyaWalletKeyringServiceObserver {
  func keyringCreated(_ keyringId: ShunyaWallet.KeyringId) { }
  
  func keyringRestored(_ keyringId: ShunyaWallet.KeyringId) { }
  
  func keyringReset() { }
  
  func locked() { }
  
  func unlocked() { }
  
  func backedUp() { }
  
  func accountsChanged() {
    update()
  }
  
  func accountsAdded(_ addedAccounts: [ShunyaWallet.AccountInfo]) {
    update()
  }
  
  func autoLockMinutesChanged() { }
  
  func selectedWalletAccountChanged(_ account: ShunyaWallet.AccountInfo) { }
  
  func selectedDappAccountChanged(_ coin: ShunyaWallet.CoinType, account: ShunyaWallet.AccountInfo?) { }
}

extension TransactionsActivityStore: ShunyaWalletTxServiceObserver {
  func onNewUnapprovedTx(_ txInfo: ShunyaWallet.TransactionInfo) {
    update()
  }
  
  func onUnapprovedTxUpdated(_ txInfo: ShunyaWallet.TransactionInfo) {
    update()
  }
  
  func onTransactionStatusChanged(_ txInfo: ShunyaWallet.TransactionInfo) {
    update()
  }
  
  func onTxServiceReset() {
    update()
  }
}

extension TransactionsActivityStore: ShunyaWalletShunyaWalletServiceObserver {
  func onActiveOriginChanged(_ originInfo: ShunyaWallet.OriginInfo) { }
  
  func onDefaultEthereumWalletChanged(_ wallet: ShunyaWallet.DefaultWallet) { }
  
  func onDefaultSolanaWalletChanged(_ wallet: ShunyaWallet.DefaultWallet) { }
  
  func onDefaultBaseCurrencyChanged(_ currency: String) { }
  
  func onDefaultBaseCryptocurrencyChanged(_ cryptocurrency: String) { }
  
  func onNetworkListChanged() {
    Task { @MainActor in
      // A network was added or removed, update our network filters for the change.
      self.networkFilters = await self.rpcService.allNetworksForSupportedCoins().map { network in
        let existingSelectionValue = self.networkFilters.first(where: { $0.model.chainId == network.chainId})?.isSelected
        return .init(isSelected: existingSelectionValue ?? true, model: network)
      }
    }
  }
  
  func onDiscoverAssetsStarted() { }
  
  func onDiscoverAssetsCompleted(_ discoveredAssets: [ShunyaWallet.BlockchainToken]) { }
  
  func onResetWallet() { }
}
