// Copyright 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import SwiftUI
import ShunyaCore
import BigNumber
import Strings
import DesignSystem

struct PendingTransactionView: View {
  @ObservedObject var confirmationStore: TransactionConfirmationStore
  let networkStore: NetworkStore
  let keyringStore: KeyringStore
  @Binding var isShowingGas: Bool
  @Binding var isShowingAdvancedSettings: Bool
  
  var onDismiss: () -> Void
  
  @Environment(\.sizeCategory) private var sizeCategory
  @Environment(\.openURL) private var openWalletURL
  
  /// Blockie size for ERC 20 Approve transactions
  @ScaledMetric private var blockieSize = 24
  /// Favicon size for ERC 20 Approve transactions
  @ScaledMetric private var faviconSize = 48
  private let maxFaviconSize: CGFloat = 96
  
  private enum ViewMode: Int {
    case transaction
    case details
  }
  
  @State private var viewMode: ViewMode = .transaction
  
  private var transactionType: String {
    if confirmationStore.activeParsedTransaction.transaction.txType == .erc20Approve {
      return Strings.Wallet.transactionTypeApprove
    }
    switch confirmationStore.activeParsedTransaction.transaction.txType {
    case .erc20Approve:
      return Strings.Wallet.transactionTypeApprove
    case .solanaDappSignTransaction, .solanaDappSignAndSendTransaction:
      return Strings.Wallet.solanaDappTransactionTitle
    default:
      return confirmationStore.activeParsedTransaction.transaction.isSwap ? Strings.Wallet.swap : Strings.Wallet.send
    }
  }
  
  /// The view for changing between available pending transactions. ex. '1 of 4 Next'
  @ViewBuilder private var transactionsButton: some View {
    if confirmationStore.unapprovedTxs.count > 1 {
      let index = confirmationStore.unapprovedTxs.firstIndex(of: confirmationStore.activeParsedTransaction.transaction) ?? 0
      HStack {
        Text(String.localizedStringWithFormat(Strings.Wallet.transactionCount, index + 1, confirmationStore.unapprovedTxs.count))
          .fontWeight(.semibold)
        Button(action: confirmationStore.nextTransaction) {
          Text(Strings.Wallet.next)
            .fontWeight(.semibold)
            .foregroundColor(Color(.shunyaBlurpleTint))
        }
      }
    }
  }
  
  /// View showing the currently selected account with a blockie
  @ViewBuilder private var accountView: some View {
    HStack {
      let address = confirmationStore.activeParsedTransaction.fromAddress
      AddressView(address: address) {
        Text(address.truncatedAddress)
          .fontWeight(.semibold)
      }
      Blockie(address: address)
        .frame(width: blockieSize, height: blockieSize)
    }
  }
  
  private var globeFavicon: some View {
    Image(systemName: "globe")
      .resizable()
      .aspectRatio(contentMode: .fit)
      .padding(8)
      .background(Color(.shunyaDisabled))
  }
  
  @ViewBuilder private var faviconAndOrigin: some View {
    VStack(spacing: 8) {
      if let originInfo = confirmationStore.originInfo {
        Group {
          if originInfo.isShunyaWalletOrigin {
            Image("wallet-shunya-icon", bundle: .module)
              .resizable()
              .aspectRatio(contentMode: .fit)
              .padding(4)
              .frame(maxWidth: .infinity, maxHeight: .infinity)
              .background(Color(.shunyaDisabled))
          } else {
            if let url = URL(string: originInfo.originSpec) {
              FaviconReader(url: url) { image in
                if let image = image {
                  Image(uiImage: image)
                    .resizable()
                } else {
                  globeFavicon
                }
              }
            } else {
              globeFavicon
            }
          }
        }
        .frame(width: min(faviconSize, maxFaviconSize), height: min(faviconSize, maxFaviconSize))
        .clipShape(RoundedRectangle(cornerRadius: 4, style: .continuous))

        Text(originInfo: originInfo)
          .font(.subheadline)
          .foregroundColor(Color(.shunyaLabel))
          .multilineTextAlignment(.center)
      }
    }
    .accessibilityElement(children: .combine)
  }
  
  /// The header displayed for an `erc20Approve` txType transaction
  @ViewBuilder private var erc20ApproveHeader: some View {
    VStack(spacing: 20) {
      VStack(spacing: 8) {
        faviconAndOrigin
        VStack(spacing: 10) {
          Text(String.localizedStringWithFormat(Strings.Wallet.confirmationViewAllowSpendTitle, confirmationStore.symbol))
            .fontWeight(.semibold)
            .foregroundColor(Color(.shunyaPrimary))
          Text(String.localizedStringWithFormat(Strings.Wallet.confirmationViewAllowSpendSubtitle, confirmationStore.symbol))
            .font(.footnote)
        }
        .multilineTextAlignment(.center)
      }
      if confirmationStore.isUnlimitedApprovalRequested {
        Label(Strings.Wallet.confirmationViewUnlimitedWarning, systemImage: "exclamationmark.triangle")
          .padding(12)
          .foregroundColor(Color(.shunyaErrorLabel))
          .font(.subheadline)
          .background(
            Color(.shunyaErrorBackground)
              .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
          )
      }
      NavigationLink(
        destination: EditPermissionsView(
          proposedAllowance: confirmationStore.proposedAllowance,
          confirmationStore: confirmationStore,
          keyringStore: keyringStore,
          networkStore: networkStore
        )
      ) {
        Text(Strings.Wallet.confirmationViewEditPermissions)
          .font(.subheadline.weight(.semibold))
          .foregroundColor(Color(.shunyaBlurpleTint))
      }
    }
    .padding(.horizontal)
    .padding(.bottom)
  }
  
  @ViewBuilder private var editGasFeeButton: some View {
    let titleView = Text(Strings.Wallet.editGasFeeButtonTitle)
      .fontWeight(.semibold)
      .foregroundColor(Color(.shunyaBlurpleTint))
    Group {
      if let gasEstimation = confirmationStore.eip1559GasEstimation {
        NavigationLink(
          destination: EditPriorityFeeView(
            transaction: confirmationStore.activeParsedTransaction.transaction,
            gasEstimation: gasEstimation,
            confirmationStore: confirmationStore
          )
        ) {
          titleView
        }
      } else {
        NavigationLink(
          destination: EditGasFeeView(
            transaction: confirmationStore.activeParsedTransaction.transaction,
            confirmationStore: confirmationStore
          )
        ) {
          titleView
        }
      }
    }
    .font(.footnote)
  }
  
  @ViewBuilder private var editNonceRow: some View {
    NavigationLink(
      destination: EditNonceView(
        confirmationStore: confirmationStore,
        transaction: confirmationStore.activeParsedTransaction.transaction
      )
    ) {
      HStack {
        Image(shunyaSystemName: "leo.settings")
          .foregroundColor(Color(.shunyaBlurpleTint))
        Text(Strings.Wallet.advancedSettingsTransaction)
          .frame(maxWidth: .infinity, alignment: .leading)
          .foregroundColor(Color(.shunyaBlurpleTint))
        Spacer()
        Image(systemName: "chevron.right")
      }
      .padding()
      .font(.footnote.weight(.semibold))
    }
  }
  
  private var currentTransactionView: some View {
    VStack {
      if confirmationStore.activeParsedTransaction.transaction.txType == .erc20Approve {
        erc20ApproveHeader
      } else {
        TransactionHeader(
          fromAccountAddress: confirmationStore.activeParsedTransaction.fromAddress,
          fromAccountName: confirmationStore.activeParsedTransaction.namedFromAddress,
          toAccountAddress: confirmationStore.activeParsedTransaction.toAddress,
          toAccountName: confirmationStore.activeParsedTransaction.namedToAddress,
          originInfo: confirmationStore.originInfo,
          transactionType: transactionType,
          value: "\(confirmationStore.value) \(confirmationStore.symbol)",
          fiat: confirmationStore.fiat
        )
      }
      
      if confirmationStore.isSolTokenTransferWithAssociatedTokenAccountCreation {
        VStack(alignment: .leading, spacing: 8) {
          Text(Strings.Wallet.confirmationViewSolSplTokenAccountCreationWarning)
            .foregroundColor(Color(.shunyaErrorLabel))
            .font(.subheadline.weight(.medium))
          Button {
            openWalletURL(WalletConstants.splTokenAccountCreationLink)
          } label: {
            Text(Strings.Wallet.learnMoreButton)
              .foregroundColor(Color(.shunyaBlurpleTint))
              .font(.subheadline)
          }
        }
        .padding(.horizontal, 24)
        .padding(.vertical, 20)
        .background(
          Color(.shunyaErrorBackground)
            .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
        )
      }
      
      // View Mode
      VStack(spacing: 12) {
        Picker("", selection: $viewMode) {
          Text(Strings.Wallet.confirmationViewModeTransaction).tag(ViewMode.transaction)
          Text(Strings.Wallet.confirmationViewModeDetails).tag(ViewMode.details)
        }
        .pickerStyle(SegmentedPickerStyle())
        Group {
          switch viewMode {
          case .transaction:
            VStack(spacing: 0) {
              if confirmationStore.activeParsedTransaction.coin == .fil {
                if let gasLimit = confirmationStore.filTxGasLimit {
                  HStack {
                    Text("Gas Limit")
                      .foregroundColor(Color(.shunyaPrimary))
                    Spacer()
                    Text("\(gasLimit) \(confirmationStore.gasSymbol)")
                      .foregroundColor(Color(.shunyaPrimary))
                      .multilineTextAlignment(.trailing)
                  }
                  .padding()
                  .accessibilityElement(children: .contain)
                  Divider()
                }
                if let gasPremium = confirmationStore.filTxGasPremium {
                  HStack {
                    Text("Gas Premium")
                      .foregroundColor(Color(.shunyaPrimary))
                    Spacer()
                    Text("\(gasPremium) \(confirmationStore.gasSymbol)")
                      .foregroundColor(Color(.shunyaPrimary))
                      .multilineTextAlignment(.trailing)
                  }
                  .padding()
                  .accessibilityElement(children: .contain)
                  Divider()
                }
                if let gasFeeCap = confirmationStore.filTxGasFeeCap {
                  HStack {
                    Text("Gas Fee Cap")
                      .foregroundColor(Color(.shunyaPrimary))
                    Spacer()
                    Text("\(gasFeeCap) \(confirmationStore.gasSymbol)")
                      .foregroundColor(Color(.shunyaPrimary))
                      .multilineTextAlignment(.trailing)
                  }
                  .padding()
                  .accessibilityElement(children: .contain)
                  Divider()
                }
              }
              HStack {
                VStack(alignment: .leading) {
                  Text(confirmationStore.activeParsedTransaction.coin == .sol ? Strings.Wallet.transactionFee : Strings.Wallet.gasFee)
                    .foregroundColor(Color(.shunyaPrimary))
                  if confirmationStore.activeParsedTransaction.coin == .eth {
                    editGasFeeButton
                  }
                }
                Spacer()
                VStack(alignment: .trailing) {
                  Text("\(confirmationStore.gasValue) \(confirmationStore.gasSymbol)")
                    .foregroundColor(Color(.shunyaPrimary))
                    .multilineTextAlignment(.trailing)
                  Text(confirmationStore.gasFiat)
                    .font(.footnote)
                }
              }
              .font(.callout)
              .padding()
              .accessibilityElement(children: .contain)
              Divider()
                .padding(.leading)
              if confirmationStore.activeParsedTransaction.transaction.txType == .erc20Approve {
                Group {
                  HStack {
                    Text(Strings.Wallet.confirmationViewCurrentAllowance)
                    Spacer()
                    Text("\(confirmationStore.currentAllowance) \(confirmationStore.symbol)")
                      .multilineTextAlignment(.trailing)
                  }
                  .padding()
                  .accessibilityElement(children: .contain)
                  Divider()
                  HStack {
                    Text(Strings.Wallet.editPermissionsProposedAllowanceHeader)
                    Spacer()
                    Text("\(confirmationStore.value) \(confirmationStore.symbol)")
                      .multilineTextAlignment(.trailing)
                  }
                  .padding()
                  .accessibilityElement(children: .contain)
                }
                .font(.callout)
                .foregroundColor(Color(.shunyaPrimary))
              } else {
                HStack {
                  Text(Strings.Wallet.total)
                    .foregroundColor(Color(.shunyaPrimary))
                    .font(.callout)
                    .accessibility(sortPriority: 1)
                  Spacer()
                  VStack(alignment: .trailing) {
                    Text(confirmationStore.activeParsedTransaction.coin == .sol ? Strings.Wallet.amountAndFee : Strings.Wallet.amountAndGas)
                      .font(.footnote)
                      .foregroundColor(Color(.secondaryShunyaLabel))
                    Text("\(confirmationStore.value) \(confirmationStore.symbol) + \(confirmationStore.gasValue) \(confirmationStore.gasSymbol)")
                      .foregroundColor(Color(.shunyaPrimary))
                      .multilineTextAlignment(.trailing)
                    HStack(spacing: 4) {
                      if !confirmationStore.isBalanceSufficient {
                        Text(Strings.Wallet.insufficientBalance)
                          .foregroundColor(Color(.shunyaErrorLabel))
                      }
                      Text(confirmationStore.totalFiat)
                        .foregroundColor(
                          confirmationStore.isBalanceSufficient ? Color(.shunyaLabel) : Color(.shunyaErrorLabel)
                        )
                    }
                    .accessibilityElement(children: .contain)
                    .font(.footnote)
                  }
                }
                .padding()
                .accessibilityElement(children: .contain)
              }
              if confirmationStore.activeParsedTransaction.coin == .eth {
                Divider()
                  .padding(.leading)
                editNonceRow
              }
            }
          case .details:
            VStack(alignment: .leading) {
              StaticTextView(text: confirmationStore.transactionDetails)
                .frame(maxWidth: .infinity)
                .frame(height: 200)
                .background(Color(.tertiaryShunyaGroupedBackground))
                .clipShape(RoundedRectangle(cornerRadius: 5, style: .continuous))
            }
            .padding()
          }
        }
        .frame(maxWidth: .infinity)
        .background(
          Color(.secondaryShunyaGroupedBackground)
        )
        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
      }
    }
  }
  
  @ViewBuilder private var rejectConfirmContainer: some View {
    if sizeCategory.isAccessibilityCategory {
      VStack {
        rejectConfirmButtons
      }
    } else {
      HStack {
        rejectConfirmButtons
      }
    }
  }
  
  @ViewBuilder private var rejectConfirmButtons: some View {
    Button(action: {
      confirmationStore.reject(transaction: confirmationStore.activeParsedTransaction.transaction) { _ in }
    }) {
      Label(Strings.Wallet.rejectTransactionButtonTitle, systemImage: "xmark")
    }
    .buttonStyle(ShunyaOutlineButtonStyle(size: .large))
    Button(action: {
      confirmationStore.isTxSubmitting = true
      confirmationStore.confirm(transaction: confirmationStore.activeParsedTransaction.transaction) { _ in }
    }) {
      Label(Strings.Wallet.confirm, systemImage: "checkmark.circle.fill")
    }
    .buttonStyle(ShunyaFilledButtonStyle(size: .large))
    .disabled(!confirmationStore.isBalanceSufficient || confirmationStore.activeTxStatus == .approved)
  }
  
  var body: some View {
    ScrollView(.vertical) {
      VStack {
        // Current network, transaction buttons
        HStack(alignment: .top) {
          if confirmationStore.activeParsedTransaction.transaction.txType != .ethSwap {
            Text(confirmationStore.network?.chainName ?? "") // network shown below each token for swap
          }
          Spacer()
          VStack(alignment: .trailing) {
            transactionsButton
            if confirmationStore.activeParsedTransaction.transaction.txType == .erc20Approve {
              accountView // for other txTypes, account is shown in `TransactionHeader`
            }
          }
        }
        .font(.callout)
        
        // Current Active Transaction info
        if confirmationStore.activeParsedTransaction.transaction.txType == .ethSwap {
          SwapTransactionConfirmationView(
            parsedTransaction: confirmationStore.activeParsedTransaction,
            network: confirmationStore.network ?? .init(),
            editGasFeeTapped: {
              isShowingGas = true
            },
            advancedSettingsTapped: {
              isShowingAdvancedSettings = true
            }
          )
        } else {
          currentTransactionView
        }
        
        // Cancel / Confirm buttons
        if confirmationStore.unapprovedTxs.count > 1 {
          Button(action: {
            confirmationStore.rejectAllTransactions { success in
              if success {
                onDismiss()
              }
            }
          }) {
            Text(String.localizedStringWithFormat(Strings.Wallet.rejectAllTransactions, confirmationStore.unapprovedTxs.count))
              .font(.subheadline.weight(.semibold))
              .foregroundColor(Color(.shunyaBlurpleTint))
          }
          .padding(.top, 8)
        }
        rejectConfirmContainer
          .padding(.top)
          .opacity(sizeCategory.isAccessibilityCategory ? 0 : 1)
          .accessibility(hidden: sizeCategory.isAccessibilityCategory)
      }
      .padding()
    }
    .overlay(
      Group {
        if sizeCategory.isAccessibilityCategory {
          rejectConfirmContainer
            .frame(maxWidth: .infinity)
            .padding(.top)
            .background(
              LinearGradient(
                stops: [
                  .init(color: Color(.shunyaGroupedBackground).opacity(0), location: 0),
                  .init(color: Color(.shunyaGroupedBackground).opacity(1), location: 0.05),
                  .init(color: Color(.shunyaGroupedBackground).opacity(1), location: 1),
                ],
                startPoint: .top,
                endPoint: .bottom
              )
              .ignoresSafeArea()
              .allowsHitTesting(false)
            )
        }
      },
      alignment: .bottom
    )
  }
}

#if DEBUG
struct PendingTransactionView_Previews: PreviewProvider {
  static var previews: some View {
    PendingTransactionView(
      confirmationStore: .previewStore,
      networkStore: .previewStore,
      keyringStore: .previewStore,
      isShowingGas: .constant(false),
      isShowingAdvancedSettings: .constant(false),
      onDismiss: { }
    )
  }
}
#endif
