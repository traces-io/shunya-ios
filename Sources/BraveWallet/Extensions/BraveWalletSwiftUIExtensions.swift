// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import SwiftUI
import ShunyaCore

extension ShunyaWallet.AccountInfo: Identifiable {
  public var id: String {
    "\(address)\(coin.rawValue)"
  }
  public var isPrimary: Bool {
    // no hardware support on iOS
    accountId.kind != .imported
  }
  
  public var isImported: Bool {
    accountId.kind == .imported
  }
  
  public var coin: ShunyaWallet.CoinType {
    accountId.coin
  }
  
  public var keyringId: ShunyaWallet.KeyringId {
    accountId.keyringId
  }
}

extension ShunyaWallet.TransactionInfo: Identifiable {
  // Already has `id` property
}

public enum AssetImageName: String {
  case ethereum = "eth-asset-icon"
  case solana = "sol-asset-icon"
  case filecoin = "filecoin-asset-icon"
  case polygon = "matic"
  case binance = "bnb-asset-icon"
  case celo = "celo"
  case avalanche = "avax"
  case fantom = "fantom"
  case aurora = "aurora"
  case optimism = "optimism"
}

extension ShunyaWallet.NetworkInfo: Identifiable {
  public var id: String {
    "\(chainId)\(coin.rawValue)"
  }
  
  var shortChainName: String {
    chainName.split(separator: " ").first?.capitalized ?? chainName
  }
  
  var isKnownTestnet: Bool {
    WalletConstants.supportedTestNetworkChainIds.contains(chainId)
  }

  public var nativeToken: ShunyaWallet.BlockchainToken {
    .init(
      contractAddress: "",
      name: symbolName,
      logo: nativeTokenLogoName ?? "",
      isErc20: false,
      isErc721: false,
      isErc1155: false,
      isNft: false,
      isSpam: false,
      symbol: symbol,
      decimals: decimals,
      visible: false,
      tokenId: "",
      coingeckoId: "",
      chainId: chainId,
      coin: coin
    )
  }
  
  public var nativeTokenLogoName: String? {
    if let logoBySymbol = assetIconNameBySymbol(symbol) {
      return logoBySymbol
    } else if let logoByChainId = assetIconNameByChainId(chainId) {
      return logoByChainId
    } else {
      return iconUrls.first
    }
  }
  
  public var nativeTokenLogoImage: UIImage? {
    guard let logo = nativeTokenLogoName else { return nil }
    return UIImage(named: logo, in: .module, with: nil)
  }
  
  public var networkLogoName: String? {
    return assetIconNameByChainId(chainId) ?? iconUrls.first
  }
  
  public var networkLogoImage: UIImage? {
    guard let logo = networkLogoName else { return nil }
    return UIImage(named: logo, in: .module, with: nil)
  }
  
  private func assetIconNameByChainId(_ chainId: String) -> String? {
    if chainId.caseInsensitiveCompare(ShunyaWallet.MainnetChainId) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.GoerliChainId) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.SepoliaChainId) == .orderedSame {
      return AssetImageName.ethereum.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.SolanaMainnet) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.SolanaDevnet) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.SolanaTestnet) == .orderedSame {
      return AssetImageName.solana.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.FilecoinMainnet) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.FilecoinTestnet) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.FilecoinEthereumMainnetChainId) == .orderedSame || chainId.caseInsensitiveCompare(ShunyaWallet.FilecoinEthereumTestnetChainId) == .orderedSame {
      return AssetImageName.filecoin.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.PolygonMainnetChainId) == .orderedSame {
      return AssetImageName.polygon.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.BinanceSmartChainMainnetChainId) == .orderedSame {
      return AssetImageName.binance.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.CeloMainnetChainId) == .orderedSame {
      return AssetImageName.celo.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.AvalancheMainnetChainId) == .orderedSame {
      return AssetImageName.avalanche.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.FantomMainnetChainId) == .orderedSame {
      return AssetImageName.fantom.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.AuroraMainnetChainId) == .orderedSame {
      return AssetImageName.aurora.rawValue
    } else if chainId.caseInsensitiveCompare(ShunyaWallet.OptimismMainnetChainId) == .orderedSame {
      return AssetImageName.optimism.rawValue
    } else {
      return nil
    }
  }
  
  private func assetIconNameBySymbol(_ symbol: String) -> String? {
    if symbol.caseInsensitiveCompare("ETH") == .orderedSame {
      return AssetImageName.ethereum.rawValue
    } else if symbol.caseInsensitiveCompare("SOL") == .orderedSame {
      return AssetImageName.solana.rawValue
    } else if symbol.caseInsensitiveCompare("FIL") == .orderedSame {
      return AssetImageName.filecoin.rawValue
    }
    return nil
  }
}

extension ShunyaWallet.BlockchainToken: Identifiable {
  public var id: String {
    contractAddress.lowercased() + chainId + symbol + tokenId
  }

  public func contractAddress(in network: ShunyaWallet.NetworkInfo) -> String {
    switch network.coin {
    case .eth:
      // ETH special swap address
      // Only checking token.symbol with selected network.symbol is sufficient
      // since there is no swap support for custom networks.
      return symbol == network.symbol ? ShunyaWallet.ethSwapAddress : contractAddress
    case .sol:
      // SOL special swap address
      // Only checking token.symbol with selected network.symbol is sufficient
      // since there is no swap support for custom networks.
      return symbol == network.symbol ? ShunyaWallet.solSwapAddress : contractAddress
    default:
      return contractAddress
    }
  }
}

extension ShunyaWallet {
  /// The address that is expected when you are swapping ETH via SwapService APIs
  public static let ethSwapAddress: String = "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
  
  /// The address that is expected when you are swapping SOL via Jupiter Swap APIs
  public static let solSwapAddress: String = "So11111111111111111111111111111111111111112"
}

extension ShunyaWallet.CoinType: Identifiable {
  public var id: Int {
    rawValue
  }
}

extension ShunyaWallet.OnRampProvider: Identifiable {
  public var id: Int {
    rawValue
  }
}

extension ShunyaWallet.OnRampCurrency: Identifiable {
  public var id: String {
    currencyCode
  }
  
  var symbol: String {
    CurrencyCode.symbol(for: currencyCode)
  }
}

extension ShunyaWallet.CoinMarket: Identifiable {
  var uniqueId: String {
    "\(symbol)\(marketCapRank)"
  }
}
