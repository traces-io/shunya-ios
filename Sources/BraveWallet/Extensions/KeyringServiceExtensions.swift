// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore
import OrderedCollections

extension ShunyaWalletKeyringService {
  
  // Fetches all keyrings for all given coin types
  func keyrings(
    for coins: OrderedSet<ShunyaWallet.CoinType>
  ) async -> [ShunyaWallet.KeyringInfo] {
    var allKeyrings: [ShunyaWallet.KeyringInfo] = []
    allKeyrings = await withTaskGroup(
      of: ShunyaWallet.KeyringInfo.self,
      returning: [ShunyaWallet.KeyringInfo].self,
      body: { @MainActor group in
        let keyringIds: [ShunyaWallet.KeyringId] = coins.flatMap(\.keyringIds)
        for keyringId in keyringIds {
          group.addTask { @MainActor in
            await self.keyringInfo(keyringId)
          }
        }
        return await group.reduce([ShunyaWallet.KeyringInfo](), { partialResult, prior in
          return partialResult + [prior]
        })
        .sorted(by: { lhs, rhs in
          if lhs.coin == .fil && rhs.coin == .fil {
            return lhs.id == ShunyaWallet.KeyringId.filecoin
          } else {
            return (lhs.coin ?? .eth).sortOrder < (rhs.coin ?? .eth).sortOrder
          }
        })
      }
    )
    return allKeyrings
  }
  
  // Fetches all keyrings for all given keyring IDs
  func keyrings(
    for keyringIds: [ShunyaWallet.KeyringId]
  ) async -> [ShunyaWallet.KeyringInfo] {
    var allKeyrings: [ShunyaWallet.KeyringInfo] = []
    allKeyrings = await withTaskGroup(
      of: ShunyaWallet.KeyringInfo.self,
      returning: [ShunyaWallet.KeyringInfo].self,
      body: { @MainActor group in
        for keyringId in keyringIds {
          group.addTask { @MainActor in
            await self.keyringInfo(keyringId)
          }
        }
        return await group.reduce([ShunyaWallet.KeyringInfo](), { partialResult, prior in
          return partialResult + [prior]
        })
        .sorted(by: { lhs, rhs in
          (lhs.coin ?? .eth).sortOrder < (rhs.coin ?? .eth).sortOrder
        })
      }
    )
    return allKeyrings
  }
  
  /// Check if any wallet account has been created given a coin type and a chain id
  @MainActor func isAccountAvailable(for coin: ShunyaWallet.CoinType, chainId: String) async -> Bool {
    let keyringId = ShunyaWallet.KeyringId.keyringId(for: coin, on: chainId)
    let keyringInfo = await keyringInfo(keyringId)
    // If user restore a wallet, `ShunyaWallet.KeyringInfo.isKeyringCreated` can be true,
    // but `ShunyaWallet.KeyringInfo.accountInfos` will be empty.
    // Hence, we will have to check if `ShunyaWallet.KeyringInfo.accountInfos` is empty instead of
    // checking the boolean of `ShunyaWallet.KeyringInfo.isKeyringCreated`
    return !keyringInfo.accountInfos.isEmpty
  }
}
