// Copyright 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import ShunyaCore
import DesignSystem
import SwiftUI

class SignTransactionRequestUnion {
  let id: Int32
  let chainId: String
  let originInfo: ShunyaWallet.OriginInfo
  let coin: ShunyaWallet.CoinType
  let fromAddress: String
  let txDatas: [ShunyaWallet.TxDataUnion]
  let rawMessage: [ShunyaWallet.ByteArrayStringUnion]
  
  init(
    id: Int32,
    chainId: String,
    originInfo: ShunyaWallet.OriginInfo,
    coin: ShunyaWallet.CoinType,
    fromAddress: String,
    txDatas: [ShunyaWallet.TxDataUnion],
    rawMessage: [ShunyaWallet.ByteArrayStringUnion]
  ) {
    self.id = id
    self.chainId = chainId
    self.originInfo = originInfo
    self.coin = coin
    self.fromAddress = fromAddress
    self.txDatas = txDatas
    self.rawMessage = rawMessage
  }
}

struct SignTransactionView: View {
  @ObservedObject var keyringStore: KeyringStore
  @ObservedObject var networkStore: NetworkStore
  
  enum Request {
    case signTransaction([ShunyaWallet.SignTransactionRequest])
    case signAllTransactions([ShunyaWallet.SignAllTransactionsRequest])
  }
  
  var request: Request
  var cryptoStore: CryptoStore
  var onDismiss: () -> Void
  
  @State private var txIndex: Int = 0
  @State private var showWarning: Bool = true
  @Environment(\.sizeCategory) private var sizeCategory
  @Environment(\.colorScheme) private var colorScheme
  @Environment(\.openURL) private var openWalletURL
  @ScaledMetric private var blockieSize = 54
  private let maxBlockieSize: CGFloat = 108
  private let normalizedRequests: [SignTransactionRequestUnion]
  
  init(
    keyringStore: KeyringStore,
    networkStore: NetworkStore,
    request: Request,
    cryptoStore: CryptoStore,
    onDismiss: @escaping () -> Void
  ) {
    self.keyringStore = keyringStore
    self.networkStore = networkStore
    self.request = request
    self.cryptoStore = cryptoStore
    self.onDismiss = onDismiss
    switch self.request {
    case .signTransaction(let requests):
      self.normalizedRequests = requests.map {
        SignTransactionRequestUnion(
          id: $0.id,
          chainId: $0.chainId,
          originInfo: $0.originInfo,
          coin: $0.coin,
          fromAddress: $0.fromAddress,
          txDatas: [$0.txData],
          rawMessage: [$0.rawMessage]
        )
      }
    case .signAllTransactions(let requests):
      self.normalizedRequests = requests.map {
        SignTransactionRequestUnion(
          id: $0.id,
          chainId: $0.chainId,
          originInfo: $0.originInfo,
          coin: $0.coin,
          fromAddress: $0.fromAddress,
          txDatas: $0.txDatas,
          rawMessage: $0.rawMessages
        )
      }
    }
  }
  
  var navigationTitle: String {
    switch request {
    case .signTransaction:
      return Strings.Wallet.signTransactionTitle
    case .signAllTransactions:
      return Strings.Wallet.signAllTransactionsTitle
    }
  }
  
  private var currentRequest: SignTransactionRequestUnion {
    normalizedRequests[txIndex]
  }
  
  private var network: ShunyaWallet.NetworkInfo? {
    networkStore.allChains.first(where: { $0.chainId == currentRequest.chainId })
  }

  private func instructionsDisplayString() -> String {
    currentRequest.txDatas
      .map { $0.solanaTxData?.instructions ?? [] }
      .map { instructionsForOneTx in
        instructionsForOneTx
          .map { TransactionParser.parseSolanaInstruction($0).toString }
          .joined(separator: "\n\n====\n\n") // separator between each instruction
      }
      .joined(separator: "\n\n\n\n") // separator between each transaction
  }

  private var account: ShunyaWallet.AccountInfo {
    keyringStore.allAccounts.first(where: { $0.address == currentRequest.fromAddress }) ?? keyringStore.selectedAccount
  }
  
  var body: some View {
    ScrollView(.vertical) {
      VStack {
        VStack(spacing: 12) {
          HStack {
            if let network = self.network {
              Text(network.chainName)
                .font(.callout)
                .foregroundColor(Color(.shunyaLabel))
            }
            Spacer()
            if normalizedRequests.count > 1 {
              HStack {
                Spacer()
                Text(String.localizedStringWithFormat(Strings.Wallet.transactionCount, txIndex + 1, normalizedRequests.count))
                  .fontWeight(.semibold)
                Button(action: next) {
                  Text(Strings.Wallet.next)
                    .fontWeight(.semibold)
                    .foregroundColor(Color(.shunyaBlurpleTint))
                }
              }
            }
          }
          VStack(spacing: 12) {
            Blockie(address: account.address)
              .frame(width: min(blockieSize, maxBlockieSize), height: min(blockieSize, maxBlockieSize))
            AddressView(address: account.address) {
              Text(account.name)
            }
            .foregroundColor(Color(.shunyaPrimary))
            .font(.callout)
            Text(originInfo: currentRequest.originInfo)
              .foregroundColor(Color(.shunyaLabel))
              .font(.subheadline)
              .multilineTextAlignment(.center)
          }
          .accessibilityElement(children: .combine)
          Text(Strings.Wallet.signatureRequestSubtitle)
            .font(.title3.weight(.semibold))
            .foregroundColor(Color(.shunyaPrimary))
        }
        .padding(.horizontal, 8)
        if showWarning {
          warningView
            .padding(.vertical, 12)
            .padding(.horizontal, 20)
        } else {
          divider
            .padding(.vertical, 8)
          VStack(alignment: .leading) {
            StaticTextView(text: instructionsDisplayString())
              .frame(maxWidth: .infinity)
              .frame(height: 200)
              .background(Color(.tertiaryShunyaGroupedBackground))
              .clipShape(RoundedRectangle(cornerRadius: 5, style: .continuous))
              .padding()
          }
          .background(
            Color(.secondaryShunyaGroupedBackground)
          )
          .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
        }
        buttonsContainer
          .padding(.top)
          .opacity(sizeCategory.isAccessibilityCategory ? 0 : 1)
          .accessibility(hidden: sizeCategory.isAccessibilityCategory)
      }
      .padding()
    }
    .navigationBarTitleDisplayMode(.inline)
    .navigationTitle(Text(navigationTitle))
    .background(Color(.shunyaGroupedBackground).edgesIgnoringSafeArea(.all))
  }
  
  @ViewBuilder private var buttonsContainer: some View {
    if sizeCategory.isAccessibilityCategory {
      VStack {
        buttons
      }
    } else {
      HStack {
        buttons
      }
    }
  }
  
  @ViewBuilder private var buttons: some View {
    if showWarning {
      cancelButton
      Button(action: { // Continue
        showWarning = false
      }) {
        Text(Strings.Wallet.continueButtonTitle)
          .imageScale(.large)
      }
      .buttonStyle(ShunyaFilledButtonStyle(size: .large))
      .disabled(txIndex != 0)
    } else {
      cancelButton
      Button(action: { // approve
        switch request {
        case .signTransaction(_):
          cryptoStore.handleWebpageRequestResponse(.signTransaction(approved: true, id: currentRequest.id))
          
        case .signAllTransactions(_):
          cryptoStore.handleWebpageRequestResponse(.signAllTransactions(approved: true, id: currentRequest.id))
        }
        if normalizedRequests.count == 1 {
          onDismiss()
        }
      }) {
        Label(Strings.Wallet.sign, shunyaSystemImage: "leo.key")
          .fixedSize(horizontal: true, vertical: false)
          .imageScale(.large)
      }
      .buttonStyle(ShunyaFilledButtonStyle(size: .large))
      .disabled(txIndex != 0)
    }
  }
  
  @ViewBuilder private var cancelButton: some View {
    Button(action: { // cancel
      switch request {
      case .signTransaction(_):
        cryptoStore.handleWebpageRequestResponse(.signTransaction(approved: false, id: currentRequest.id))
      case .signAllTransactions(_):
        cryptoStore.handleWebpageRequestResponse(.signAllTransactions(approved: false, id: currentRequest.id))
      }
      if normalizedRequests.count == 1 {
        onDismiss()
      }
    }) {
      Label(Strings.cancelButtonTitle, systemImage: "xmark")
        .fixedSize(horizontal: true, vertical: false)
        .imageScale(.large)
    }
    .buttonStyle(ShunyaOutlineButtonStyle(size: .large))
  }
  
  @ViewBuilder private var divider: some View {
    VStack {
      Text(Strings.Wallet.solanaSignTransactionDetails)
        .font(.subheadline.weight(.semibold))
        .foregroundColor(Color(.shunyaPrimary))
      HStack {
        LinearGradient(shunyaGradient: colorScheme == .dark ? .darkGradient02 : .lightGradient02)
      }
      .frame(height: 4)
    }
  }
  
  @ViewBuilder private var warningView: some View {
    VStack(alignment: .leading, spacing: 8) {
      Group {
        Label(Strings.Wallet.signTransactionSignRisk, systemImage: "exclamationmark.triangle")
          .font(.subheadline.weight(.semibold))
          .foregroundColor(Color(.shunyaErrorLabel))
          .padding(.top, 12)
        Text(Strings.Wallet.solanaSignTransactionWarning)
          .font(.subheadline)
          .foregroundColor(Color(.shunyaErrorLabel))
        Button(action: {
          openWalletURL(WalletConstants.signTransactionRiskLink)
        }) {
          Text(Strings.Wallet.learnMoreButton)
            .font(.subheadline)
            .foregroundColor(Color(.shunyaBlurpleTint))
        }
        .padding(.bottom, 12)
      }
      .padding(.horizontal, 12)
    }
    .background(
      Color(.shunyaErrorBackground)
        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
    )
  }
  
  private func next() {
    if txIndex + 1 < normalizedRequests.count {
      txIndex += 1
    } else {
      txIndex = 0
    }
  }
}

#if DEBUG
struct SignTransaction_Previews: PreviewProvider {
  static var previews: some View {
    SignTransactionView(
      keyringStore: .previewStore,
      networkStore: .previewStore,
      request: .signTransaction([ShunyaWallet.SignTransactionRequest(
        originInfo: .init(),
        id: 0,
        from: ShunyaWallet.AccountInfo.previewAccount.accountId,
        fromAddress: ShunyaWallet.AccountInfo.previewAccount.address,
        txData: .init(),
        rawMessage: .init(),
        coin: .sol,
        chainId: ShunyaWallet.SolanaMainnet
      )]),
      cryptoStore: .previewStore,
      onDismiss: {}
    )
    .previewColorSchemes()
  }
}
#endif
