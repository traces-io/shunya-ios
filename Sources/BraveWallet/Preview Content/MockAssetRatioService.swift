// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore

#if DEBUG

class MockAssetRatioService: ShunyaWalletAssetRatioService {
  private let assets: [String: ShunyaWallet.AssetPrice] = [
    "eth": .init(fromAsset: "eth", toAsset: "usd", price: "3059.99", assetTimeframeChange: "-57.23"),
    "bat": .init(fromAsset: "bat", toAsset: "usd", price: "0.627699", assetTimeframeChange: "-0.019865"),
  ]
  func price(_ fromAssets: [String], toAssets: [String], timeframe: ShunyaWallet.AssetPriceTimeframe, completion: @escaping (Bool, [ShunyaWallet.AssetPrice]) -> Void) {
    let prices = assets.filter { (key, value) in
      fromAssets.contains(where: { key == $0 })
    }
    completion(!prices.isEmpty, Array(prices.values))
  }

  func estimatedTime(_ gasPrice: String, completion: @escaping (Bool, String) -> Void) {
    completion(false, "")
  }

  func tokenInfo(_ contractAddress: String, completion: @escaping (ShunyaWallet.BlockchainToken?) -> Void) {
    completion(nil)
  }

  func priceHistory(_ asset: String, vsAsset: String, timeframe: ShunyaWallet.AssetPriceTimeframe, completion: @escaping (Bool, [ShunyaWallet.AssetTimePrice]) -> Void) {
    completion(false, [])
  }
  
  func buyUrlV1(_ provider: ShunyaWallet.OnRampProvider, chainId: String, address: String, symbol: String, amount: String, currencyCode: String, completion: @escaping (String, String?) -> Void) {
    completion("", nil)
  }
  
  func sellUrl(_ provider: ShunyaWallet.OffRampProvider, chainId: String, address: String, symbol: String, amount: String, currencyCode: String, completion: @escaping (String, String?) -> Void) {
    completion("", nil)
  }
  
  func coinMarkets(_ vsAsset: String, limit: UInt8, completion: @escaping (Bool, [ShunyaWallet.CoinMarket]) -> Void) {
    completion(false, [])
  }
}

#endif
