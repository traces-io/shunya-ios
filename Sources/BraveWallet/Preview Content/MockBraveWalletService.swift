// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore

#if DEBUG
/// A test wallet service that implements some basic functionality for the use of SwiftUI Previews.
///
/// - note: Do not use this directly, use ``NetworkStore.previewStore``
class MockShunyaWalletService: ShunyaWalletShunyaWalletService {
  private var assets: [String: [ShunyaWallet.BlockchainToken]] = [
    ShunyaWallet.MainnetChainId: [.previewToken],
    ShunyaWallet.GoerliChainId: [.previewToken],
  ]
  private var defaultCurrency = CurrencyCode.usd
  private var defaultCryptocurrency = "eth"
  private var coin: ShunyaWallet.CoinType = .eth

  func userAssets(_ chainId: String, coin: ShunyaWallet.CoinType, completion: @escaping ([ShunyaWallet.BlockchainToken]) -> Void) {
    completion(assets[chainId] ?? [])
  }
  
  func allUserAssets(_ completion: @escaping ([ShunyaWallet.BlockchainToken]) -> Void) {
    let allAssets = assets.values.flatMap { $0 }
    completion(Array(allAssets))
  }

  func addUserAsset(_ token: ShunyaWallet.BlockchainToken, completion: @escaping (Bool) -> Void) {
    assets[token.chainId]?.append(token)
  }

  func removeUserAsset(_ token: ShunyaWallet.BlockchainToken, completion: @escaping (Bool) -> Void) {
    assets[token.chainId]?.removeAll(where: { $0.contractAddress == token.contractAddress })
  }

  func setUserAssetVisible(_ token: ShunyaWallet.BlockchainToken, visible: Bool, completion: @escaping (Bool) -> Void) {
    let chainAssets = assets[token.chainId]
    if let index = chainAssets?.firstIndex(where: { $0.contractAddress == token.contractAddress }) {
      chainAssets?[index].visible = visible
    }
  }

  func `import`(from type: ShunyaWallet.ExternalWalletType, password: String, newPassword: String, completion: @escaping (Bool, String?) -> Void) {
    completion(false, nil)
  }

  func defaultWallet(_ completion: @escaping (ShunyaWallet.DefaultWallet) -> Void) {
    completion(.shunyaWallet)
  }

  func hasEthereumPermission(_ origin: URLOrigin, account: String, completion: @escaping (Bool, Bool) -> Void) {
    completion(false, false)
  }

  func resetEthereumPermission(_ origin: URLOrigin, account: String, completion: @escaping (Bool) -> Void) {
    completion(false)
  }

  func activeOrigin(_ completion: @escaping (ShunyaWallet.OriginInfo) -> Void) {
    completion(.init())
  }

  func pendingSignMessageRequests(_ completion: @escaping ([ShunyaWallet.SignMessageRequest]) -> Void) {
    completion([])
  }

  func pendingAddSuggestTokenRequests(_ completion: @escaping ([ShunyaWallet.AddSuggestTokenRequest]) -> Void) {
    completion([])
  }

  func defaultBaseCurrency(_ completion: @escaping (String) -> Void) {
    completion(defaultCurrency.code)
  }

  func setDefaultBaseCurrency(_ currency: String) {
    defaultCurrency = CurrencyCode(code: currency)
  }

  func defaultBaseCryptocurrency(_ completion: @escaping (String) -> Void) {
    completion(defaultCryptocurrency)
  }

  func setDefaultBaseCryptocurrency(_ cryptocurrency: String) {
    defaultCryptocurrency = cryptocurrency
  }

  func isExternalWalletInstalled(_ type: ShunyaWallet.ExternalWalletType, completion: @escaping (Bool) -> Void) {
    completion(false)
  }

  func isExternalWalletInitialized(_ type: ShunyaWallet.ExternalWalletType, completion: @escaping (Bool) -> Void) {
    completion(false)
  }

  func addEthereumPermission(_ origin: URLOrigin, account: String, completion: @escaping (Bool) -> Void) {
    completion(false)
  }

  func add(_ observer: ShunyaWalletShunyaWalletServiceObserver) {
  }
  
  func add(_ observer: ShunyaWalletShunyaWalletServiceTokenObserver) {
  }
  
  func setDefaultWallet(_ defaultEthWallet: ShunyaWallet.DefaultWallet) {
  }

  func notifySignMessageRequestProcessed(_ approved: Bool, id: Int32, signature: ShunyaWallet.ByteArrayStringUnion?, error: String?) {
  }

  func notifySignMessageHardwareRequestProcessed(_ approved: Bool, id: Int32, signature: String, error: String) {
  }

  func notifyAddSuggestTokenRequestsProcessed(_ approved: Bool, contractAddresses: [String]) {
  }

  func reset() {
  }
  
  func activeOrigin(_ completion: @escaping (String, String) -> Void) {
    completion("", "")
  }
  
  func notifyGetPublicKeyRequestProcessed(_ requestId: String, approved: Bool) {
  }
  
  func pendingGetEncryptionPublicKeyRequests() async -> [ShunyaWallet.GetEncryptionPublicKeyRequest] {
    return []
  }
  
  func notifyDecryptRequestProcessed(_ requestId: String, approved: Bool) {
  }
  
  func pendingDecryptRequests() async -> [ShunyaWallet.DecryptRequest] {
    return []
  }
  
  func showWalletTestNetworks(_ completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func selectedCoin(_ completion: @escaping (ShunyaWallet.CoinType) -> Void) {
    completion(coin)
  }
  
  func setSelectedCoin(_ coin: ShunyaWallet.CoinType) {
    self.coin = coin
  }
  
  func addPermission(_ accountId: ShunyaWallet.AccountId, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func hasPermission(_ accounts: [ShunyaWallet.AccountId], completion: @escaping (Bool, [ShunyaWallet.AccountId]) -> Void) {
    completion(false, [])
  }
  
  func resetPermission(_ accountId: ShunyaWallet.AccountId, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func isBase58EncodedSolanaPubkey(_ key: String, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func eTldPlusOne(fromOrigin origin: URLOrigin, completion: @escaping (ShunyaWallet.OriginInfo) -> Void) {
    completion(.init())
  }
  
  func webSites(withPermission coin: ShunyaWallet.CoinType, completion: @escaping ([String]) -> Void) {
    completion([])
  }
  
  func resetWebSitePermission(_ coin: ShunyaWallet.CoinType, formedWebsite: String, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func pendingSignTransactionRequests(_ completion: @escaping ([ShunyaWallet.SignTransactionRequest]) -> Void) {
    completion([])
  }
  
  func pendingSignAllTransactionsRequests(_ completion: @escaping ([ShunyaWallet.SignAllTransactionsRequest]) -> Void) {
    completion([])
  }
  
  func notifySignTransactionRequestProcessed(_ approved: Bool, id: Int32, signature: ShunyaWallet.ByteArrayStringUnion?, error: String?) {
  }
  
  func notifySignAllTransactionsRequestProcessed(_ approved: Bool, id: Int32, signatures: [ShunyaWallet.ByteArrayStringUnion]?, error: String?) {
  }
  
  func base58Encode(_ addresses: [[NSNumber]], completion: @escaping ([String]) -> Void) {
    completion([])
  }

  func isPermissionDenied(_ coin: ShunyaWallet.CoinType, completion: @escaping (Bool) -> Void) {
    completion(false)
  }

  func onOnboardingShown() {
  }
  
  func defaultEthereumWallet(_ completion: @escaping (ShunyaWallet.DefaultWallet) -> Void) {
    completion(.shunyaWallet)
  }
  
  func defaultSolanaWallet(_ completion: @escaping (ShunyaWallet.DefaultWallet) -> Void) {
    completion(.shunyaWallet)
  }
  
  func setDefaultEthereumWallet(_ defaultEthWallet: ShunyaWallet.DefaultWallet) {
  }
  
  func setDefaultSolanaWallet(_ defaultEthWallet: ShunyaWallet.DefaultWallet) {
  }
  
  func discoverAssetsOnAllSupportedChains() {
  }
  
  func nftDiscoveryEnabled(_ completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func setNftDiscoveryEnabled(_ enabled: Bool) {
  }
  
  func chainId(forActiveOrigin coin: ShunyaWallet.CoinType, completion: @escaping (String) -> Void) {
    completion(ShunyaWallet.MainnetChainId)
  }
  
  func setChainIdForActiveOrigin(_ coin: ShunyaWallet.CoinType, chainId: String, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func balanceScannerSupportedChains(_ completion: @escaping ([String]) -> Void) {
    completion([])
  }

  func discoverEthAllowances(_ completion: @escaping ([ShunyaWallet.AllowanceInfo]) -> Void) {
    completion([])
  }
  
  func setAssetSpamStatus(_ token: ShunyaWallet.BlockchainToken, status: Bool, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func simpleHashSpamNfTs(_ walletAddress: String, chainIds: [String], coin: ShunyaWallet.CoinType, cursor: String?, completion: @escaping ([ShunyaWallet.BlockchainToken], String?) -> Void) {
    completion([], nil)
  }
  
  func ensureSelectedAccount(forChain coin: ShunyaWallet.CoinType, chainId: String, completion: @escaping (ShunyaWallet.AccountId?) -> Void) {
    completion(nil)
  }
  
  func networkForSelectedAccount(onActiveOrigin completion: @escaping (ShunyaWallet.NetworkInfo?) -> Void) {
    completion(nil)
  }
  
  func setNetworkForSelectedAccountOnActiveOrigin(_ chainId: String, completion: @escaping (Bool) -> Void) {
    completion(false)
  }
  
  func convertFevm(toFvmAddress isMainnet: Bool, fevmAddresses: [String], completion: @escaping ([String: String]) -> Void) {
    completion([:])
  }
}
#endif
