// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore

#if DEBUG

class MockTxService: ShunyaWalletTxService {
  private let txs: [ShunyaWallet.TransactionInfo] = [
    .previewConfirmedERC20Approve,
    .previewConfirmedSend,
    .previewConfirmedSwap
  ]
  
  func transactionInfo(_ coinType: ShunyaWallet.CoinType, chainId: String, txMetaId: String, completion: @escaping (ShunyaWallet.TransactionInfo?) -> Void) {
    completion(txs.first(where: { $0.id == txMetaId }))
  }

  func addUnapprovedTransaction(_ txDataUnion: ShunyaWallet.TxDataUnion, from: ShunyaWallet.AccountId, origin: URLOrigin?, groupId: String?, completion: @escaping (Bool, String, String) -> Void) {
    completion(true, "txMetaId", "")
  }

  func rejectTransaction(_ coinType: ShunyaWallet.CoinType, chainId: String, txMetaId: String, completion: @escaping (Bool) -> Void) {
  }

  func allTransactionInfo(_ coinType: ShunyaWallet.CoinType, chainId: String?, from: ShunyaWallet.AccountId?, completion: @escaping ([ShunyaWallet.TransactionInfo]) -> Void) {
    completion(txs.map { tx in
      tx.txStatus = .unapproved
      return tx
    })
  }

  func add(_ observer: ShunyaWalletTxServiceObserver) {
  }

  func speedupOrCancelTransaction(_ coinType: ShunyaWallet.CoinType, chainId: String, txMetaId: String, cancel: Bool, completion: @escaping (Bool, String, String) -> Void) {
    completion(false, "", "Error Message")
  }

  func retryTransaction(_ coinType: ShunyaWallet.CoinType, chainId: String, txMetaId: String, completion: @escaping (Bool, String, String) -> Void) {
    completion(false, "", "Error Message")
  }

  func pendingTransactionsCount(_ completion: @escaping (UInt32) -> Void) {
    completion(UInt32(txs.count))
  }
  
  func reset() {
  }
  
  func transactionMessage(toSign coinType: ShunyaWallet.CoinType, chainId: String, txMetaId: String, completion: @escaping (ShunyaWallet.MessageToSignUnion?) -> Void) {
    completion(ShunyaWallet.MessageToSignUnion(messageStr: "Mock transaction message"))
  }

  func approveTransaction(_ coinType: ShunyaWallet.CoinType, chainId: String, txMetaId: String, completion: @escaping (Bool, ShunyaWallet.ProviderErrorUnion, String) -> Void) {
    completion(false, .init(providerError: .internalError), "Error Message")
  }
}

#endif
