// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
import ShunyaCore
import ShunyaShared
import ShunyaUI

#if DEBUG

extension WalletStore {
  static var previewStore: WalletStore {
    .init(
      keyringService: MockKeyringService(),
      rpcService: MockJsonRpcService(),
      walletService: MockShunyaWalletService(),
      assetRatioService: MockAssetRatioService(),
      swapService: MockSwapService(),
      blockchainRegistry: MockBlockchainRegistry(),
      txService: MockTxService(),
      ethTxManagerProxy: MockEthTxManagerProxy(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      ipfsApi: TestIpfsAPI()
    )
  }
}

extension CryptoStore {
  static var previewStore: CryptoStore {
    .init(
      keyringService: MockKeyringService(),
      rpcService: MockJsonRpcService(),
      walletService: MockShunyaWalletService(),
      assetRatioService: MockAssetRatioService(),
      swapService: MockSwapService(),
      blockchainRegistry: MockBlockchainRegistry(),
      txService: MockTxService(),
      ethTxManagerProxy: MockEthTxManagerProxy(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      ipfsApi: TestIpfsAPI()
    )
  }
}

extension NetworkStore {
  static var previewStore: NetworkStore {
    .init(
      keyringService: MockKeyringService(),
      rpcService: MockJsonRpcService(),
      walletService: MockShunyaWalletService(),
      swapService: MockSwapService(),
      userAssetManager: TestableWalletUserAssetManager()
    )
  }
  
  static var previewStoreWithCustomNetworkAdded: NetworkStore {
    let store = NetworkStore.previewStore
    store.addCustomNetwork(
      .init(
        chainId: "0x100",
        chainName: "MockChain",
        blockExplorerUrls: ["https://mockchainscan.com"],
        iconUrls: [],
        activeRpcEndpointIndex: 0,
        rpcEndpoints: [URL(string: "https://rpc.mockchain.com")!],
        symbol: "MOCK",
        symbolName: "MOCK",
        decimals: 18,
        coin: .eth,
        supportedKeyrings: [ShunyaWallet.KeyringId.default.rawValue].map(NSNumber.init(value:)),
        isEip1559: false
      )
    ) { _, _ in }
    return store
  }
}

extension KeyringStore {
  static var previewStore: KeyringStore {
    .init(keyringService: MockKeyringService(),
          walletService: MockShunyaWalletService(),
          rpcService: MockJsonRpcService()
    )
  }
  static var previewStoreWithWalletCreated: KeyringStore {
    let store = KeyringStore(keyringService: MockKeyringService(), walletService: MockShunyaWalletService(), rpcService: MockJsonRpcService())
    store.createWallet(password: "password")
    return store
  }
}

extension BuyTokenStore {
  static var previewStore: BuyTokenStore {
    .init(
      blockchainRegistry: MockBlockchainRegistry(),
      keyringService: MockKeyringService(),
      rpcService: MockJsonRpcService(),
      walletService: ShunyaWallet.TestShunyaWalletService.previewWalletService,
      assetRatioService: ShunyaWallet.TestAssetRatioService.previewAssetRatioService,
      prefilledToken: .previewToken
    )
  }
}

extension SendTokenStore {
  static var previewStore: SendTokenStore {
    .init(
      keyringService: MockKeyringService(),
      rpcService: MockJsonRpcService(),
      walletService: MockShunyaWalletService(),
      txService: MockTxService(),
      blockchainRegistry: MockBlockchainRegistry(),
      assetRatioService: MockAssetRatioService(),
      ethTxManagerProxy: MockEthTxManagerProxy(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      prefilledToken: .previewToken,
      ipfsApi: TestIpfsAPI(),
      userAssetManager: TestableWalletUserAssetManager()
    )
  }
}

extension AssetDetailStore {
  static var previewStore: AssetDetailStore {
    .init(
      assetRatioService: MockAssetRatioService(),
      keyringService: MockKeyringService(),
      rpcService: MockJsonRpcService(),
      walletService: MockShunyaWalletService(),
      txService: MockTxService(),
      blockchainRegistry: MockBlockchainRegistry(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      swapService: MockSwapService(),
      userAssetManager: TestableWalletUserAssetManager(),
      assetDetailType: .blockchainToken(.previewToken)
    )
  }
}

extension SwapTokenStore {
  static var previewStore: SwapTokenStore {
    .init(
      keyringService: MockKeyringService(),
      blockchainRegistry: MockBlockchainRegistry(),
      rpcService: MockJsonRpcService(),
      swapService: MockSwapService(),
      txService: MockTxService(),
      walletService: MockShunyaWalletService(),
      ethTxManagerProxy: MockEthTxManagerProxy(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      userAssetManager: TestableWalletUserAssetManager(),
      prefilledToken: nil
    )
  }
}

extension UserAssetsStore {
  static var previewStore: UserAssetsStore {
    .init(
      blockchainRegistry: MockBlockchainRegistry(),
      rpcService: MockJsonRpcService(),
      keyringService: MockKeyringService(),
      assetRatioService: MockAssetRatioService(),
      ipfsApi: TestIpfsAPI(),
      userAssetManager: TestableWalletUserAssetManager()
    )
  }
}

extension AccountActivityStore {
  static var previewStore: AccountActivityStore {
    .init(
      account: .previewAccount,
      observeAccountUpdates: false,
      keyringService: MockKeyringService(),
      walletService: MockShunyaWalletService(),
      rpcService: MockJsonRpcService(),
      assetRatioService: MockAssetRatioService(),
      txService: MockTxService(),
      blockchainRegistry: MockBlockchainRegistry(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      ipfsApi: TestIpfsAPI(),
      userAssetManager: TestableWalletUserAssetManager()
    )
  }
}

extension TransactionConfirmationStore {
  static var previewStore: TransactionConfirmationStore {
    .init(
      assetRatioService: MockAssetRatioService(),
      rpcService: MockJsonRpcService(),
      txService: MockTxService(),
      blockchainRegistry: MockBlockchainRegistry(),
      walletService: MockShunyaWalletService(),
      ethTxManagerProxy: MockEthTxManagerProxy(),
      keyringService: {
        let service = MockKeyringService()
        service.createWallet("password") { _  in }
        return service
      }(),
      solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
      userAssetManager: TestableWalletUserAssetManager()
    )
  }
}

extension SettingsStore {
  static var previewStore: SettingsStore {
    .init(
      keyringService: MockKeyringService(),
      walletService: MockShunyaWalletService(),
      rpcService: MockJsonRpcService(),
      txService: MockTxService(),
      ipfsApi: TestIpfsAPI(),
      keychain: TestableKeychain()
    )
  }
}

extension TabDappStore {
  static var previewStore: TabDappStore {
    .init()
  }
}

extension TransactionsActivityStore {
  static let preview: TransactionsActivityStore = .init(
    keyringService: MockKeyringService(),
    rpcService: MockJsonRpcService(),
    walletService: MockShunyaWalletService(),
    assetRatioService: MockAssetRatioService(),
    blockchainRegistry: MockBlockchainRegistry(),
    txService: MockTxService(),
    solTxManagerProxy: ShunyaWallet.TestSolanaTxManagerProxy.previewProxy,
    userAssetManager: TestableWalletUserAssetManager()
  )
}

extension ShunyaWallet.TestSolanaTxManagerProxy {
  static var previewProxy: ShunyaWallet.TestSolanaTxManagerProxy {
    let solTxManagerProxy = ShunyaWallet.TestSolanaTxManagerProxy()
    solTxManagerProxy._makeSystemProgramTransferTxData = { _, _, _, completion in
      completion(.init(), .success, "")
    }
    solTxManagerProxy._makeTokenProgramTransferTxData = {_, _, _, _, _, completion in
      completion(.init(), .success, "")
    }
    solTxManagerProxy._estimatedTxFee = { _, _, completion in
      completion(UInt64(0), .success, "")
    }
    
    return solTxManagerProxy
  }
}

extension ShunyaWallet.TestShunyaWalletService {
  static var previewWalletService: ShunyaWallet.TestShunyaWalletService {
    let walletService = ShunyaWallet.TestShunyaWalletService()
    return walletService
  }
}

extension ShunyaWallet.TestAssetRatioService {
  static var previewAssetRatioService: ShunyaWallet.TestAssetRatioService {
    let assetRatioService = ShunyaWallet.TestAssetRatioService()
    assetRatioService._buyUrlV1 = { _, _, _, _, _, _, completion in
      completion("", nil)
    }
    
    return assetRatioService
  }
}

#endif
