// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import SwiftUI
import Strings
import ShunyaUI
import Preferences
import ShunyaCore

public struct Web3SettingsView: View {
  var settingsStore: SettingsStore?
  var networkStore: NetworkStore?
  var keyringStore: KeyringStore?
  
  @ObservedObject var enableIPFSResourcesResolver = Preferences.Wallet.resolveIPFSResources

  @State private var isShowingResetWalletAlert = false
  @State private var isShowingResetTransactionAlert = false
  /// If we are showing the modal so the user can enter their password to enable unlock via biometrics.
  @State private var isShowingBiometricsPasswordEntry = false
  @State private var ipfsNFTGatewayURL: String = ""
  @State private var ipfsGatewayURL: String = ""
  
  public init(
    settingsStore: SettingsStore? = nil,
    networkStore: NetworkStore? = nil,
    keyringStore: KeyringStore? = nil
  ) {
    self.settingsStore = settingsStore
    self.networkStore = networkStore
    self.keyringStore = keyringStore
  }
  
  public var body: some View {
    List {
      if let settingsStore = settingsStore {
        if let networkStore = networkStore, let keyringStore = keyringStore {
          WalletSettingsView(
            settingsStore: settingsStore,
            networkStore: networkStore,
            keyringStore: keyringStore,
            isShowingResetWalletAlert: $isShowingResetWalletAlert,
            isShowingResetTransactionAlert: $isShowingResetTransactionAlert,
            isShowingBiometricsPasswordEntry: $isShowingBiometricsPasswordEntry
          )
        }
        // means users come from the browser not the wallet
        Section(
          header: Text(Strings.Wallet.ipfsSettingsHeader)
        ) {
          Picker(selection: $enableIPFSResourcesResolver.value) {
            ForEach(Preferences.Wallet.Web3IPFSOption.allCases) { option in
              Text(option.name)
                .foregroundColor(Color(.secondaryShunyaLabel))
                .tag(option)
            }
          } label: {
            VStack(alignment: .leading, spacing: 6) {
              Text(Strings.Wallet.ipfsResourcesOptionsTitle)
                .foregroundColor(Color(.shunyaLabel))
              Text(LocalizedStringKey(String.localizedStringWithFormat(Strings.Wallet.ipfsResolveMethodDescription, WalletConstants.ipfsLearnMoreLink.absoluteString)))
                .foregroundColor(Color(.secondaryShunyaLabel))
                .tint(Color(.shunyaBlurpleTint))
                .font(.footnote)
            }
          }
          .listRowBackground(Color(.secondaryShunyaGroupedBackground))
          NavigationLink(destination: IPFSCustomGatewayView(ipfsAPI: settingsStore.ipfsApi)) {
            VStack(alignment: .leading, spacing: 6) {
              Text(Strings.Wallet.ipfsPublicGatewayAddressTitle)
                .foregroundColor(Color(.shunyaLabel))
              Text(ipfsGatewayURL)
                .font(.footnote)
                .foregroundColor(Color(.secondaryShunyaLabel))
            }
            .padding(.vertical, 4)
          }
          .listRowBackground(Color(.secondaryShunyaGroupedBackground))
          NavigationLink(destination: IPFSCustomGatewayView(ipfsAPI: settingsStore.ipfsApi, isForNFT: true)) {
            VStack(alignment: .leading, spacing: 6) {
              Text(Strings.Wallet.ipfsPublicGatewayAddressNFTTitle)
                .foregroundColor(Color(.shunyaLabel))
              Text(ipfsNFTGatewayURL)
                .font(.footnote)
                .foregroundColor(Color(.secondaryShunyaLabel))
            }
            .padding(.vertical, 4)
          }
          .listRowBackground(Color(.secondaryShunyaGroupedBackground))
        }
        
      }
      if let settingsStore {
        Web3DomainSettingsView(settingsStore: settingsStore)
      }
    }
    .listStyle(InsetGroupedListStyle())
    .listBackgroundColor(Color(UIColor.shunyaGroupedBackground))
    .navigationTitle(Strings.Wallet.web3)
    .navigationBarTitleDisplayMode(.inline)
    .background(
      Color.clear
        .alert(isPresented: $isShowingResetTransactionAlert) {
          Alert(
            title: Text(Strings.Wallet.settingsResetTransactionAlertTitle),
            message: Text(Strings.Wallet.settingsResetTransactionAlertMessage),
            primaryButton: .destructive(
              Text(Strings.Wallet.settingsResetTransactionAlertButtonTitle),
              action: {
                settingsStore?.resetTransaction()
              }),
            secondaryButton: .cancel(Text(Strings.cancelButtonTitle))
          )
        }
    )
    .background(
      Color.clear
        .alert(isPresented: $isShowingResetWalletAlert) {
          Alert(
            title: Text(Strings.Wallet.settingsResetWalletAlertTitle),
            message: Text(Strings.Wallet.settingsResetWalletAlertMessage),
            primaryButton: .destructive(
              Text(Strings.Wallet.settingsResetWalletAlertButtonTitle),
              action: {
                settingsStore?.reset()
              }),
            secondaryButton: .cancel(Text(Strings.no))
          )
        }
    )
    .background(
      Color.clear
        .sheet(isPresented: $isShowingBiometricsPasswordEntry) {
          if let keyringStore {
            BiometricsPasscodeEntryView(keyringStore: keyringStore)
          }
        }
    )
    .onAppear {
      if let urlString = settingsStore?.ipfsApi.nftIpfsGateway?.absoluteString, urlString != ipfsNFTGatewayURL {
        ipfsNFTGatewayURL = urlString
      }
      if let urlString = settingsStore?.ipfsApi.ipfsGateway?.absoluteString, urlString != ipfsGatewayURL {
        ipfsGatewayURL = urlString
      }
      settingsStore?.setup()
    }
  }
}

private struct WalletSettingsView: View {
  @ObservedObject var settingsStore: SettingsStore
  @ObservedObject var networkStore: NetworkStore
  @ObservedObject var keyringStore: KeyringStore
  @ObservedObject var displayDappsNotifications = Preferences.Wallet.displayWeb3Notifications
  
  @Binding var isShowingResetWalletAlert: Bool
  @Binding var isShowingResetTransactionAlert: Bool
  @Binding var isShowingBiometricsPasswordEntry: Bool

  private var autoLockIntervals: [AutoLockInterval] {
    var all = AutoLockInterval.allOptions
    if !all.contains(settingsStore.autoLockInterval) {
      // Ensure that the users selected interval always appears as an option even if
      // we remove it from `allOptions`
      all.append(settingsStore.autoLockInterval)
    }
    return all.sorted(by: { $0.value < $1.value })
  }

  var body: some View {
    if keyringStore.isDefaultKeyringCreated {
      sections
    } else {
      // `KeyringStore` is optional in `Web3SettingsView`, but observed here.
      // When wallet is reset, we need SwiftUI to be notified `isDefaultKeyringCreated`
      // changed so we can hide Wallet specific sections
      EmptyView()
    }
  }
  
  @ViewBuilder private var sections: some View {
    Section(
      footer: Text(Strings.Wallet.autoLockFooter)
        .foregroundColor(Color(.secondaryShunyaLabel))
    ) {
      Picker(selection: $settingsStore.autoLockInterval) {
        ForEach(autoLockIntervals) { interval in
          Text(interval.label)
            .foregroundColor(Color(.secondaryShunyaLabel))
            .tag(interval)
        }
      } label: {
        Text(Strings.Wallet.autoLockTitle)
          .foregroundColor(Color(.shunyaLabel))
          .padding(.vertical, 4)
      }
      .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
    Section {
      Picker(selection: $settingsStore.currencyCode) {
        ForEach(CurrencyCode.allCurrencyCodes) { currencyCode in
          Text(currencyCode.code)
            .foregroundColor(Color(.secondaryShunyaLabel))
            .tag(currencyCode)
        }
      } label: {
        Text(Strings.Wallet.settingsDefaultBaseCurrencyTitle)
          .foregroundColor(Color(.shunyaLabel))
          .padding(.vertical, 4)
      }
      .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
    if settingsStore.isBiometricsAvailable, keyringStore.defaultKeyring.isKeyringCreated {
      Section(
        footer: Text(Strings.Wallet.settingsEnableBiometricsFooter)
          .foregroundColor(Color(.secondaryShunyaLabel))
      ) {
        Toggle(
          Strings.Wallet.settingsEnableBiometricsTitle,
          isOn: Binding(get: { settingsStore.isBiometricsUnlockEnabled },
                        set: { toggledBiometricsUnlock($0) })
        )
        .foregroundColor(Color(.shunyaLabel))
        .toggleStyle(SwitchToggleStyle(tint: Color(.shunyaBlurpleTint)))
        .listRowBackground(Color(.secondaryShunyaGroupedBackground))
      }
    }
    Section(
      footer: Text(LocalizedStringKey(String.localizedStringWithFormat(Strings.Wallet.web3SettingsEnableNFTDiscoveryFooter, WalletConstants.nftDiscoveryURL.absoluteDisplayString)))
        .foregroundColor(Color(.secondaryShunyaLabel))
        .tint(Color(.shunyaBlurpleTint))
    ) {
      Toggle(Strings.Wallet.web3SettingsEnableNFTDiscovery, isOn: $settingsStore.isNFTDiscoveryEnabled)
        .foregroundColor(Color(.shunyaLabel))
        .toggleStyle(SwitchToggleStyle(tint: Color(.shunyaBlurpleTint)))
        .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
    Section(
      footer: Text(Strings.Wallet.networkFooter)
        .foregroundColor(Color(.secondaryShunyaLabel))
    ) {
      NavigationLink(destination: CustomNetworkListView(networkStore: networkStore)) {
        Text(Strings.Wallet.settingsNetworkButtonTitle)
          .foregroundColor(Color(.shunyaLabel))
      }
      .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
    Section(
      header: Text(Strings.Wallet.web3PreferencesSectionTitle)
        .foregroundColor(Color(.secondaryShunyaLabel))
    ) {
      Group {
        ForEach(WalletConstants.supportedCoinTypes(.dapps)) { coin in
          NavigationLink(
            destination:
              DappsSettings(
                coin: coin,
                siteConnectionStore: settingsStore.manageSiteConnectionsStore(keyringStore: keyringStore)
              )
              .onDisappear {
                settingsStore.closeManageSiteConnectionStore()
              }
          ) {
            Text(coin.localizedTitle)
              .foregroundColor(Color(.shunyaLabel))
          }
        }
        Toggle(Strings.Wallet.web3PreferencesDisplayWeb3Notifications, isOn: $displayDappsNotifications.value)
          .foregroundColor(Color(.shunyaLabel))
          .toggleStyle(SwitchToggleStyle(tint: Color(.shunyaBlurpleTint)))
      }
      .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
    Section(
      footer: Text(Strings.Wallet.settingsResetTransactionFooter)
        .foregroundColor(Color(.secondaryShunyaLabel))
    ) {
      Button(action: { isShowingResetTransactionAlert = true }) {
        Text(Strings.Wallet.settingsResetTransactionTitle)
          .foregroundColor(Color(.shunyaBlurpleTint))
      }
      .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
    Section {
      Button(action: { isShowingResetWalletAlert = true }) {
        Text(Strings.Wallet.settingsResetButtonTitle)
          .foregroundColor(.red)
      } // iOS 15: .role(.destructive)
    }
    .listRowBackground(Color(.secondaryShunyaGroupedBackground))
  }
  
  private func toggledBiometricsUnlock(_ enabled: Bool) {
    if enabled {
      self.isShowingBiometricsPasswordEntry = true
    } else {
      keyringStore.resetKeychainStoredPassword()
    }
  }
}

#if DEBUG
struct WalletSettingsView_Previews: PreviewProvider {
  static var previews: some View {
    NavigationView {
      WalletSettingsView(
        settingsStore: .previewStore,
        networkStore: .previewStore,
        keyringStore: .previewStore,
        isShowingResetWalletAlert: .constant(false),
        isShowingResetTransactionAlert: .constant(false),
        isShowingBiometricsPasswordEntry: .constant(false)
      )
    }
  }
}
#endif

/*
 Section containing the follow preferences:
 - Allow SNS Resolve (Ask/Enabled/Disabled)
 - Allow ENS Resolve (Ask/Enabled/Disabled)
 - Allow ENS Offchain Resolve (Ask/Enabled/Disabled)
 */
private struct Web3DomainSettingsView: View {

  @ObservedObject var settingsStore: SettingsStore

  @Environment(\.openURL) private var openWalletURL

  var body: some View {
    Section(header: Text(Strings.Wallet.web3DomainOptionsHeader)) {
      Group {
        snsResolveMethodPreference
        ensResolveMethodPreference
        ensOffchainResolveMethodPreference
        udResolveMethodPreference
      }
      .listRowBackground(Color(.secondaryShunyaGroupedBackground))
    }
  }
  
  @ViewBuilder private var ensResolveMethodPreference: some View {
    Picker(selection: $settingsStore.ensResolveMethod) {
      ForEach(ShunyaWallet.ResolveMethod.allCases) { option in
        Text(option.name)
          .foregroundColor(Color(.secondaryShunyaLabel))
          .tag(option)
      }
    } label: {
      Text(Strings.Wallet.ensResolveMethodTitle)
        .foregroundColor(Color(.shunyaLabel))
        .padding(.vertical, 4)
    }
  }
  
  @ViewBuilder private var ensOffchainResolveMethodPreference: some View {
    Picker(selection: $settingsStore.ensOffchainResolveMethod) {
      ForEach(ShunyaWallet.ResolveMethod.allCases) { option in
        Text(option.name)
          .foregroundColor(Color(.secondaryShunyaLabel))
          .tag(option)
      }
    } label: {
      VStack(alignment: .leading, spacing: 6) {
        Text(Strings.Wallet.ensOffchainResolveMethodTitle)
          .foregroundColor(Color(.shunyaLabel))
        Text(LocalizedStringKey(String.localizedStringWithFormat(Strings.Wallet.ensOffchainResolveMethodDescription, WalletConstants.shunyaWalletENSOffchainURL.absoluteDisplayString)))
          .foregroundColor(Color(.secondaryShunyaLabel))
          .tint(Color(.shunyaBlurpleTint))
          .font(.footnote)
      }
      .padding(.vertical, 4)
    }
  }
  
  @ViewBuilder private var snsResolveMethodPreference: some View {
    Picker(selection: $settingsStore.snsResolveMethod) {
      ForEach(ShunyaWallet.ResolveMethod.allCases) { option in
        Text(option.name)
          .foregroundColor(Color(.secondaryShunyaLabel))
          .tag(option)
      }
    } label: {
      Text(Strings.Wallet.snsResolveMethodTitle)
        .foregroundColor(Color(.shunyaLabel))
        .padding(.vertical, 4)
    }
  }
  
  @ViewBuilder private var udResolveMethodPreference: some View {
    Picker(selection: $settingsStore.udResolveMethod) {
      ForEach(ShunyaWallet.ResolveMethod.allCases) { option in
        Text(option.name)
          .foregroundColor(Color(.secondaryShunyaLabel))
          .tag(option)
      }
    } label: {
      VStack(alignment: .leading, spacing: 6) {
        Text(Strings.Wallet.udResolveMethodTitle)
          .foregroundColor(Color(.shunyaLabel))
        Text(LocalizedStringKey(String.localizedStringWithFormat(Strings.Wallet.udResolveMethodDescription, WalletConstants.shunyaWalletUnstoppableDomainsURL.absoluteDisplayString)))
          .foregroundColor(Color(.secondaryShunyaLabel))
          .tint(Color(.shunyaBlurpleTint))
          .font(.footnote)
      }
      .padding(.vertical, 4)
    }
  }
}
