// Copyright 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import Foundation
import Data
import ShunyaCore
import Preferences
import CoreData

public protocol WalletUserAssetManagerType: AnyObject {
  func getAllVisibleAssetsInNetworkAssets(networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets]
  func getAllUserAssetsInNetworkAssets(networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets]
  func getUserAsset(_ asset: ShunyaWallet.BlockchainToken) -> WalletUserAsset?
  func addUserAsset(_ asset: ShunyaWallet.BlockchainToken, completion: (() -> Void)?)
  func removeUserAsset(_ asset: ShunyaWallet.BlockchainToken, completion: (() -> Void)?)
  func removeGroup(for groupId: String, completion: (() -> Void)?)
  func updateUserAsset(for asset: ShunyaWallet.BlockchainToken, visible: Bool, completion: (() -> Void)?)
}

public class WalletUserAssetManager: WalletUserAssetManagerType {
  
  private let rpcService: ShunyaWalletJsonRpcService
  private let walletService: ShunyaWalletShunyaWalletService
  
  public init(
    rpcService: ShunyaWalletJsonRpcService,
    walletService: ShunyaWalletShunyaWalletService
  ) {
    self.rpcService = rpcService
    self.walletService = walletService
  }
  
  public func getAllUserAssetsInNetworkAssets(networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets] {
    var allVisibleUserAssets: [NetworkAssets] = []
    for (index, network) in networks.enumerated() {
      let groupId = network.walletUserAssetGroupId
      if let walletUserAssets = WalletUserAssetGroup.getGroup(groupId: groupId)?.walletUserAssets {
        let networkAsset = NetworkAssets(
          network: network,
          tokens: walletUserAssets.map({ $0.blockchainToken }),
          sortOrder: index
        )
        allVisibleUserAssets.append(networkAsset)
      }
    }
    return allVisibleUserAssets.sorted(by: { $0.sortOrder < $1.sortOrder })
  }
  
  public func getAllVisibleAssetsInNetworkAssets(networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets] {
    var allVisibleUserAssets: [NetworkAssets] = []
    for (index, network) in networks.enumerated() {
      let groupId = network.walletUserAssetGroupId
      if let walletUserAssets = WalletUserAssetGroup.getGroup(groupId: groupId)?.walletUserAssets?.filter(\.visible) {
        let networkAsset = NetworkAssets(
          network: network,
          tokens: walletUserAssets.map({ $0.blockchainToken }),
          sortOrder: index
        )
        allVisibleUserAssets.append(networkAsset)
      }
    }
    return allVisibleUserAssets.sorted(by: { $0.sortOrder < $1.sortOrder })
  }
  
  public func getUserAsset(_ asset: ShunyaWallet.BlockchainToken) -> WalletUserAsset? {
    WalletUserAsset.getUserAsset(asset: asset)
  }
  
  public func addUserAsset(_ asset: ShunyaWallet.BlockchainToken, completion: (() -> Void)?) {
    guard WalletUserAsset.getUserAsset(asset: asset) == nil else {
      completion?()
      return
    }
    WalletUserAsset.addUserAsset(asset: asset, completion: completion)
  }
  
  public func removeUserAsset(_ asset: ShunyaWallet.BlockchainToken, completion: (() -> Void)?) {
    WalletUserAsset.removeUserAsset(asset: asset, completion: completion)
  }
  
  public func updateUserAsset(for asset: ShunyaWallet.BlockchainToken, visible: Bool, completion: (() -> Void)?) {
    WalletUserAsset.updateUserAsset(for: asset, visible: visible, completion: completion)
  }
  
  public func removeGroup(for groupId: String, completion: (() -> Void)?) {
    WalletUserAssetGroup.removeGroup(groupId, completion: completion)
  }
  
  public func migrateUserAssets(completion: (() -> Void)? = nil) {
    Task { @MainActor in
      if !Preferences.Wallet.migrateCoreToWalletUserAssetCompleted.value {
        migrateUserAssets(for: Array(WalletConstants.supportedCoinTypes()), completion: completion)
      } else {
        let allNetworks = await rpcService.allNetworksForSupportedCoins(respectTestnetPreference: false)
        DataController.performOnMainContext { context in
          let newCoins = self.allNewCoinsIntroduced(networks: allNetworks, context: context)
          if !newCoins.isEmpty {
            self.migrateUserAssets(for: newCoins, completion: completion)
          } else {
            completion?()
          }
        }
      }
    }
  }
  
  private func allNewCoinsIntroduced(networks: [ShunyaWallet.NetworkInfo], context: NSManagedObjectContext) -> [ShunyaWallet.CoinType] {
    guard let assetGroupIds = WalletUserAssetGroup.getAllGroups(context: context)?.map({ group in
      group.groupId
    }) else { return WalletConstants.supportedCoinTypes().elements }
    var newCoins: Set<ShunyaWallet.CoinType> = []
    for network in networks where !assetGroupIds.contains("\(network.coin.rawValue).\(network.chainId)") {
      newCoins.insert(network.coin)
    }
    return Array(newCoins)
  }
  
  private func migrateUserAssets(for coins: [ShunyaWallet.CoinType], completion: (() -> Void)?) {
    Task { @MainActor in
      var fetchedUserAssets: [String: [ShunyaWallet.BlockchainToken]] = [:]
      let networks: [ShunyaWallet.NetworkInfo] = await rpcService.allNetworks(for: coins, respectTestnetPreference: false)
      let networkAssets = await walletService.allUserAssets(in: networks)
      for networkAsset in networkAssets {
        fetchedUserAssets["\(networkAsset.network.coin.rawValue).\(networkAsset.network.chainId)"] = networkAsset.tokens
      }
      WalletUserAsset.migrateVisibleAssets(fetchedUserAssets) {
        Preferences.Wallet.migrateCoreToWalletUserAssetCompleted.value = true
        completion?()
      }
    }
  }
}

#if DEBUG
public class TestableWalletUserAssetManager: WalletUserAssetManagerType {
  public var _getAllVisibleAssetsInNetworkAssets: ((_ networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets])?
  public var _getAllUserAssetsInNetworkAssets: ((_ networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets])?
  
  public init() {}
  
  public func getAllUserAssetsInNetworkAssets(networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets] {
    let defaultAssets: [NetworkAssets] = [
      NetworkAssets(network: .mockMainnet, tokens: [.previewToken], sortOrder: 0),
      NetworkAssets(network: .mockGoerli, tokens: [.previewToken], sortOrder: 1)
    ]
    let chainIds = networks.map { $0.chainId }
    return _getAllUserAssetsInNetworkAssets?(networks) ?? defaultAssets.filter({
      chainIds.contains($0.network.chainId)
    })
  }
  
  public func getAllVisibleAssetsInNetworkAssets(networks: [ShunyaWallet.NetworkInfo]) -> [NetworkAssets] {
    _getAllVisibleAssetsInNetworkAssets?(networks) ?? []
  }
  
  public func getUserAsset(_ asset: ShunyaWallet.BlockchainToken) -> WalletUserAsset? {
    return nil
  }
  
  public func addUserAsset(_ asset: ShunyaWallet.BlockchainToken, completion: (() -> Void)?) {
  }
  
  public func removeUserAsset(_ asset: ShunyaWallet.BlockchainToken, completion: (() -> Void)?) {
  }

  public func removeGroup(for groupId: String, completion: (() -> Void)?) {
  }
  
  public func updateUserAsset(for asset: ShunyaWallet.BlockchainToken, visible: Bool, completion: (() -> Void)?) {
  }
}
#endif
