/* Copyright 2021 The Shunya Authors. All rights reserved.
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import SwiftUI

/// Styles a SwiftUI `TextField` to use some of the aspects from Shunya's design system.
///
/// We cannot style the placeholder unfortunately.
///
/// - Warning: This uses a private SwiftUI API which may break in the future (or if Apple ever provides a
///            non-private version of this API). Should be checked at each major OS release. If it breaks,
///            change to a regular `ViewModifier`
public struct ShunyaTextFieldStyle: TextFieldStyle {
  public init() {}

  public func _body(configuration: TextField<_Label>) -> some View {
    configuration
      .modifier(BaseShunyaTextFieldStyleModifier())
  }
}

/// Styles a SwiftUI `TextField` to use some of the aspects from Shunya's design system which can display
/// an error below it.
///
/// We cannot style the placeholder unfortunately.
///
/// - Warning: This uses a private SwiftUI API which may break in the future (or if Apple ever provides a
///            non-private version of this API). Should be checked at each major OS release. If it breaks,
///            change to a regular `ViewModifier`
public struct ShunyaValidatedTextFieldStyle<Failure: LocalizedError & Equatable>: TextFieldStyle {
  public var error: Failure?

  /// Creates a validated TextField style that displays a red border & background color when the provided
  /// error is non-nil.
  public init(error: Failure?) {
    self.error = error
  }

  /// Creates a validated TextField style when the error provided is equal to a specific case of that Error
  ///
  /// This allows you to write the following:
  ///
  ///   enum FormError: LocalizedError {
  ///     case malformedData
  ///   }
  ///
  ///   TextField(...)
  ///     .textFieldStyle(ShunyaValidatedTextFieldStyle(error, when: .malformedData)
  public init(error: Failure?, when predicate: Failure) {
    self.error = error == predicate ? error : nil
  }

  public func _body(configuration: TextField<_Label>) -> some View {
    VStack(alignment: .leading) {
      configuration
        .modifier(
          BaseShunyaTextFieldStyleModifier(
            strokeColor: error != nil ? Color(.shunyaErrorBorder) : nil,
            lineWidthFactor: error != nil ? 2 : nil,
            backgroundColor: error != nil ? Color(.shunyaErrorBackground) : nil
          )
        )
      if let error = error {
        HStack(alignment: .firstTextBaseline, spacing: 4) {
          Image(systemName: "exclamationmark.circle.fill")
          Text(error.localizedDescription)
            .fixedSize(horizontal: false, vertical: true)
            .animation(nil, value: error.localizedDescription)  // Dont animate the text change, just alpha
        }
        .transition(
          .asymmetric(
            insertion: .opacity.animation(.default),
            removal: .identity
          )
        )
        .font(.footnote)
        .foregroundColor(Color(.shunyaErrorLabel))
        .padding(.leading, 8)
      }
    }
  }
}

private struct BaseShunyaTextFieldStyleModifier: ViewModifier {
  @Environment(\.pixelLength) private var pixelLength

  var strokeColor: Color?
  var lineWidthFactor: CGFloat?
  var backgroundColor: Color?

  private var borderShape: some InsettableShape {
    RoundedRectangle(cornerRadius: 4, style: .continuous)
  }

  func body(content: Content) -> some View {
    content
      .font(.callout)
      .padding(.vertical, 10)
      .padding(.horizontal, 12)
      .overlay(
        borderShape
          // * 2 + clipShape below = pixel perfect hairline border
          .stroke(strokeColor ?? Color(.secondaryButtonTint), lineWidth: 2 * (lineWidthFactor ?? 1))
      )
      .background(
        backgroundColor ?? Color(.shunyaBackground)
      )
      .clipShape(borderShape)
  }
}
