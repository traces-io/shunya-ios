// Copyright 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import SwiftUI
import Shared
import ShunyaUI
import ShunyaStrings

public struct OnboardingBottomBarView: View {
  public var dismiss: (() -> Void)?
  public var switchBottomBar: (() -> Void)?

  public init() {}

  public var body: some View {
    VStack() {
      Button {
        dismiss?()
      } label: {
        Image(shunyaSystemName: "leo.close")
          .renderingMode(.template)
          .foregroundColor(Color(.shunyaPrimary))
      }
      .frame(maxWidth: .infinity, alignment: .trailing)
      VStack(spacing: 24) {
        Image("bottom-bar-logo-bottom", bundle: .module)
          .aspectRatio(contentMode: .fit)
        Text(Strings.Callout.bottomBarCalloutTitle)
          .font(.title2.weight(.semibold))
          .foregroundColor(Color(.shunyaPrimary))
          .multilineTextAlignment(.center)
        Text(Strings.Callout.bottomBarCalloutDescription)
          .font(.body)
          .multilineTextAlignment(.center)
          .foregroundColor(Color(.shunyaPrimary))
          .padding(.horizontal, 16)
      }
      .padding(.bottom, 16)
      Button(action: {
        switchBottomBar?()
      }) {
        Text(Strings.Callout.bottomBarCalloutButtonTitle)
          .frame(maxWidth: .infinity, maxHeight: .infinity)
          .font(.title3.weight(.medium))
          .padding()
      }
      .frame(height: 44)
      .background(Color(.shunyaBlurple))
      .accentColor(Color(.white))
      .clipShape(Capsule())
      .padding(.horizontal, 16)
      Button(action: {
        dismiss?()
      }) {
        Text(Strings.Callout.bottomBarCalloutDismissButtonTitle)
          .frame(maxWidth: .infinity, maxHeight: .infinity)
          .font(.title3.weight(.medium))
          .foregroundColor(Color(.shunyaPrimary))
      }
      .frame(height: 44)
      .background(Color(.clear))
      .accentColor(Color(.white))
    }
    .frame(maxWidth: ShunyaUX.baseDimensionValue)
    .padding()
    .background(Color(.shunyaBackground))
    .accessibilityEmbedInScrollView()
  }
}

#if DEBUG
struct OnboardingBottomBarView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      ShunyaUI.PopupView {
        OnboardingBottomBarView()
      }
      .previewDevice("iPhone 12 Pro")

      ShunyaUI.PopupView {
        OnboardingBottomBarView()
      }
      .previewDevice("iPad Pro (9.7-inch)")
    }
  }
}
#endif
