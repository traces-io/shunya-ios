// Copyright 2022 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Foundation
@testable import ShunyaTalk
import Shared
import XCTest

class ShunyaTalkTests: XCTestCase {
  @MainActor func testShunyaTalkJitsiIntegrationEnabledOnRelease() {
    AppConstants.buildChannel = .release
    XCTAssertTrue(ShunyaTalkJitsiCoordinator.isIntegrationEnabled)
  }
  
  @MainActor func testShunyaTalkJitsiIntegrationEnabledOnBeta() {
    AppConstants.buildChannel = .beta
    XCTAssertTrue(ShunyaTalkJitsiCoordinator.isIntegrationEnabled)
  }
}
