import XCTest
import ShunyaCore
@testable import ShunyaWallet

class ShunyaWalletExtensionsTests: XCTestCase {
  
  func testIsNativeAsset() {
    let ethCoin: ShunyaWallet.BlockchainToken = .init(contractAddress: "", name: "Eth", logo: "", isErc20: false, isErc721: false, isErc1155: false, isNft: false, isSpam: false, symbol: "Eth", decimals: 18, visible: true, tokenId: "", coingeckoId: "", chainId: "", coin: .eth)
    let ethToken: ShunyaWallet.BlockchainToken = .init(contractAddress: "mock-eth-contract-address", name: "Eth Token", logo: "", isErc20: true, isErc721: false, isErc1155: false, isNft: false, isSpam: false, symbol: "ETHTOKEN", decimals: 18, visible: false, tokenId: "", coingeckoId: "", chainId: "", coin: .eth)
    let ethNetwork = ShunyaWallet.NetworkInfo.mockMainnet
    
    XCTAssertTrue(ethNetwork.isNativeAsset(ethCoin))
    XCTAssertFalse(ethNetwork.isNativeAsset(ethToken))
    
    let solCoin: ShunyaWallet.BlockchainToken = .init(contractAddress: "", name: "Sol", logo: "", isErc20: false, isErc721: false, isErc1155: false, isNft: false, isSpam: false, symbol: "SOL", decimals: 9, visible: true, tokenId: "", coingeckoId: "", chainId: "", coin: .sol)
    let solToken: ShunyaWallet.BlockchainToken = .init(contractAddress: "mock-sol-contract-address", name: "Sol Token", logo: "", isErc20: true, isErc721: false, isErc1155: false, isNft: false, isSpam: false, symbol: "SOLTOKEN", decimals: 9, visible: false, tokenId: "", coingeckoId: "", chainId: "", coin: .sol)
    let solNetwork = ShunyaWallet.NetworkInfo.mockSolana
    
    XCTAssertTrue(solNetwork.isNativeAsset(solCoin))
    XCTAssertFalse(solNetwork.isNativeAsset(solToken))
  }
}
