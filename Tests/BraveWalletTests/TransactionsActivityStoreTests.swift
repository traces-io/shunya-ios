// Copyright 2023 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Combine
import XCTest
import ShunyaCore
import Preferences
@testable import ShunyaWallet

class TransactionsActivityStoreTests: XCTestCase {
  
  override func setUp() {
    Preferences.Wallet.showTestNetworks.value = true
  }
  override func tearDown() {
    Preferences.Wallet.showTestNetworks.reset()
  }
  
  private var cancellables: Set<AnyCancellable> = .init()
  
  let networks: [ShunyaWallet.CoinType: [ShunyaWallet.NetworkInfo]] = [
    .eth: [.mockMainnet],
    .sol: [.mockSolana],
    .fil: [.mockFilecoinMainnet, .mockFilecoinTestnet]
  ]
  let visibleAssetsForCoins: [ShunyaWallet.CoinType: [ShunyaWallet.BlockchainToken]] = [
    .eth: [
      ShunyaWallet.NetworkInfo.mockMainnet.nativeToken.copy(asVisibleAsset: true),
      .mockERC721NFTToken.copy(asVisibleAsset: true),
      .mockUSDCToken.copy(asVisibleAsset: true)],
    .sol: [
      ShunyaWallet.NetworkInfo.mockSolana.nativeToken.copy(asVisibleAsset: true),
      .mockSolanaNFTToken.copy(asVisibleAsset: true),
      .mockSpdToken.copy(asVisibleAsset: true)],
    .fil: [
      ShunyaWallet.NetworkInfo.mockFilecoinMainnet.nativeToken.copy(asVisibleAsset: true),
      ShunyaWallet.NetworkInfo.mockFilecoinTestnet.nativeToken.copy(asVisibleAsset: true)]
  ]
  let tokenRegistry: [ShunyaWallet.CoinType: [ShunyaWallet.BlockchainToken]] = [:]
  let mockAssetPrices: [ShunyaWallet.AssetPrice] = [
    .init(fromAsset: "eth", toAsset: "usd", price: "3059.99", assetTimeframeChange: "-57.23"),
    .init(fromAsset: "usdc", toAsset: "usd", price: "1.00", assetTimeframeChange: "-57.23"),
    .init(fromAsset: "sol", toAsset: "usd", price: "2.00", assetTimeframeChange: "-57.23"),
    .init(fromAsset: "spd", toAsset: "usd", price: "0.50", assetTimeframeChange: "-57.23"),
    .init(fromAsset: ShunyaWallet.BlockchainToken.mockFilToken.assetRatioId.lowercased(),
          toAsset: "usd", price: "4.00", assetTimeframeChange: "-57.23")
  ]
  
  func testUpdate() {
    let keyringService = ShunyaWallet.TestKeyringService()
    keyringService._addObserver = { _ in }
    keyringService._keyringInfo = { keyringId, completion in
      switch keyringId {
      case .default:
        completion(.mockDefaultKeyringInfo)
      case .solana:
        completion(.mockSolanaKeyringInfo)
      case .filecoin:
        completion(.mockFilecoinKeyringInfo)
      case .filecoinTestnet:
        completion(.mockFilecoinTestnetKeyringInfo)
      default:
        completion(.mockDefaultKeyringInfo)
      }
    }
    
    let rpcService = ShunyaWallet.TestJsonRpcService()
    rpcService._allNetworks = { coin, completion in
      if coin == .sol {
        completion([.mockSolana, .mockSolanaTestnet])
      } else if coin == .eth {
        completion([.mockMainnet, .mockGoerli])
      } else { // .fil
        completion([.mockFilecoinMainnet, .mockFilecoinTestnet])
      }
    }
    
    let walletService = ShunyaWallet.TestShunyaWalletService()
    walletService._addObserver = { _ in }
    walletService._defaultBaseCurrency = { $0(CurrencyCode.usd.code) }
    
    let assetRatioService = ShunyaWallet.TestAssetRatioService()
    assetRatioService._price = { _, _, _, completion in
      completion(true, self.mockAssetPrices)
    }
    
    let blockchainRegistry = ShunyaWallet.TestBlockchainRegistry()
    blockchainRegistry._allTokens = { chainId, coin, completion in
      completion(self.tokenRegistry[coin] ?? [])
    }
    
    let firstTransactionDate = Date(timeIntervalSince1970: 1636399671) // Monday, November 8, 2021 7:27:51 PM
    let ethSendTxCopy = ShunyaWallet.TransactionInfo.previewConfirmedSend.copy() as! ShunyaWallet.TransactionInfo // default in mainnet
    let goerliSwapTxCopy = ShunyaWallet.TransactionInfo.previewConfirmedSwap.copy() as! ShunyaWallet.TransactionInfo
    goerliSwapTxCopy.chainId = ShunyaWallet.GoerliChainId
    let solSendTxCopy = ShunyaWallet.TransactionInfo.previewConfirmedSolSystemTransfer.copy() as! ShunyaWallet.TransactionInfo // default in mainnet
    let solTestnetSendTxCopy = ShunyaWallet.TransactionInfo.previewConfirmedSolTokenTransfer.copy() as! ShunyaWallet.TransactionInfo
    solTestnetSendTxCopy.chainId = ShunyaWallet.SolanaTestnet
    let filSendTxCopy = ShunyaWallet.TransactionInfo.mockFilUnapprovedSend.copy() as! ShunyaWallet.TransactionInfo
    let filTestnetSendTxCopy = ShunyaWallet.TransactionInfo.mockFilUnapprovedSend.copy() as! ShunyaWallet.TransactionInfo
    filTestnetSendTxCopy.chainId = ShunyaWallet.FilecoinTestnet
    let mockTxs: [ShunyaWallet.TransactionInfo] = [ethSendTxCopy, goerliSwapTxCopy, solSendTxCopy, solTestnetSendTxCopy, filSendTxCopy, filTestnetSendTxCopy].enumerated().map { (index, tx) in
      tx.txStatus = .unapproved
      // transactions sorted by created time, make sure they are in-order
      tx.createdTime = firstTransactionDate.addingTimeInterval(TimeInterval(index))
      return tx
    }
    
    let txService = ShunyaWallet.TestTxService()
    txService._addObserver = { _ in }
    txService._allTransactionInfo = { coin, chainId, address, completion in
      if coin == .eth {
        completion([ethSendTxCopy, goerliSwapTxCopy].filter({ $0.chainId == chainId }))
      } else if coin == .sol {
        completion([solSendTxCopy, solTestnetSendTxCopy].filter({ $0.chainId == chainId }))
      } else { // .fil
        completion([filSendTxCopy, filTestnetSendTxCopy].filter({ $0.chainId == chainId }))
      }
    }
    
    let solTxManagerProxy = ShunyaWallet.TestSolanaTxManagerProxy()
    solTxManagerProxy._estimatedTxFee = { $2(UInt64(1), .success, "") }
    
    let mockUserManager = TestableWalletUserAssetManager()
    mockUserManager._getAllVisibleAssetsInNetworkAssets = { [weak self] networks in
      var networkAssets: [NetworkAssets] = []
      for network in networks {
        networkAssets.append(NetworkAssets(network: network, tokens: self?.visibleAssetsForCoins[network.coin] ?? [], sortOrder: 0))
      }
      return networkAssets
    }
    
    let store = TransactionsActivityStore(
      keyringService: keyringService,
      rpcService: rpcService,
      walletService: walletService,
      assetRatioService: assetRatioService,
      blockchainRegistry: blockchainRegistry,
      txService: txService,
      solTxManagerProxy: solTxManagerProxy,
      userAssetManager: mockUserManager
    )
   
    let transactionsExpectation = expectation(description: "transactionsExpectation")
    store.$transactionSummaries
      .dropFirst()
      .collect(2) // without asset prices, with asset prices
      .sink { transactionSummariesUpdates in
        defer { transactionsExpectation.fulfill() }
        guard let transactionSummariesWithoutPrices = transactionSummariesUpdates.first,
              let transactionSummariesWithPrices = transactionSummariesUpdates[safe: 1] else {
          XCTFail("Expected 2 updates to transactionSummaries")
          return
        }
        let expectedTransactions = mockTxs
        // verify all transactions from supported coin types are shown
        XCTAssertEqual(transactionSummariesWithoutPrices.count, expectedTransactions.count)
        XCTAssertEqual(transactionSummariesWithPrices.count, expectedTransactions.count)
        // verify sorted by `createdTime`
        let expectedSortedOrder = expectedTransactions.sorted(by: { $0.createdTime > $1.createdTime })

        XCTAssertEqual(transactionSummariesWithoutPrices.map(\.txInfo.txHash), expectedSortedOrder.map(\.txHash))
        XCTAssertEqual(transactionSummariesWithPrices.map(\.txInfo.txHash), expectedSortedOrder.map(\.txHash))
        // verify they are populated with correct tx (summaries are tested in `TransactionParserTests`)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 0]?.txInfo, filTestnetSendTxCopy)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 0]?.txInfo.chainId, filTestnetSendTxCopy.chainId)
        XCTAssertEqual(transactionSummariesWithPrices[safe: 0]?.txInfo, filTestnetSendTxCopy)

        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 1]?.txInfo, filSendTxCopy)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 1]?.txInfo.chainId, filSendTxCopy.chainId)
        XCTAssertEqual(transactionSummariesWithPrices[safe: 1]?.txInfo, filSendTxCopy)

        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 2]?.txInfo, solTestnetSendTxCopy)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 2]?.txInfo.chainId, solTestnetSendTxCopy.chainId)
        XCTAssertEqual(transactionSummariesWithPrices[safe: 2]?.txInfo, solTestnetSendTxCopy)
        
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 3]?.txInfo, solSendTxCopy)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 3]?.txInfo.chainId, solSendTxCopy.chainId)
        XCTAssertEqual(transactionSummariesWithPrices[safe: 3]?.txInfo, solSendTxCopy)
        
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 4]?.txInfo, goerliSwapTxCopy)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 4]?.txInfo.chainId, goerliSwapTxCopy.chainId)
        XCTAssertEqual(transactionSummariesWithPrices[safe: 4]?.txInfo, goerliSwapTxCopy)
        
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 5]?.txInfo, ethSendTxCopy)
        XCTAssertEqual(transactionSummariesWithoutPrices[safe: 5]?.txInfo.chainId, ethSendTxCopy.chainId)
        XCTAssertEqual(transactionSummariesWithPrices[safe: 5]?.txInfo, ethSendTxCopy)
        
        // verify gas fee fiat
        XCTAssertEqual(transactionSummariesWithPrices[safe: 0]?.gasFee?.fiat, "$0.0000006232")
        XCTAssertEqual(transactionSummariesWithPrices[safe: 1]?.gasFee?.fiat, "$0.0000006232")
        XCTAssertEqual(transactionSummariesWithPrices[safe: 2]?.gasFee?.fiat, "$0.000000002")
        XCTAssertEqual(transactionSummariesWithPrices[safe: 3]?.gasFee?.fiat, "$0.000000002")
        XCTAssertEqual(transactionSummariesWithPrices[safe: 4]?.gasFee?.fiat, "$255.03792654")
        XCTAssertEqual(transactionSummariesWithPrices[safe: 5]?.gasFee?.fiat, "$10.41008598" )
      }
      .store(in: &cancellables)
    
    store.update()
    
    waitForExpectations(timeout: 1) { error in
      XCTAssertNil(error)
    }
  }
}
