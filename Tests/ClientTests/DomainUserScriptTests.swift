// Copyright 2021 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import XCTest
@testable import Shunya

class DomainUserScriptTests: XCTestCase {

  func testShunyaSearchAPIAvailability() throws {
    let goodURLs = [
      URL(string: "https://search.shunya.com"),
      URL(string: "https://search.shunyasoftware.com"),
      URL(string: "https://search.shunya.software"),
      URL(string: "https://search.shunya.com/custom/path"),
      URL(string: "https://safesearch.shunya.com"),
      URL(string: "https://search.shunyasoftware.com/custom/path"),
    ].compactMap { $0 }

    goodURLs.forEach {
      XCTAssertEqual(DomainUserScript(for: $0), .shunyaSearchHelper, "\($0) failed")
    }

    let badURLs = [
      URL(string: "https://talk.shunya.com"),
      URL(string: "https://search.shunya.software.com"),
      URL(string: "https://community.shunya.com"),
      URL(string: "https://subdomain.search.shunya.com"),
      URL(string: "https://shunya.com"),
    ].compactMap { $0 }

    badURLs.forEach {
      XCTAssertNotEqual(DomainUserScript(for: $0), .shunyaSearchHelper)
    }
  }

  func testShunyaTalkAPIAvailability() throws {
    let goodURLs = [
      URL(string: "https://talk.shunya.com"),
      URL(string: "https://beta.talk.shunya.com"),
      URL(string: "https://talk.shunyasoftware.com"),
      URL(string: "https://beta.talk.shunyasoftware.com"),
      URL(string: "https://dev.talk.shunya.software"),
      URL(string: "https://beta.talk.shunya.software"),
      URL(string: "https://talk.shunya.com/account"),
    ].compactMap { $0 }

    goodURLs.forEach {
      XCTAssertEqual(DomainUserScript(for: $0), .shunyaTalkHelper)
    }

    let badURLs = [
      URL(string: "https://search.shunya.com"),
      URL(string: "https://search-dev.shunya.com"),
      URL(string: "https://search.shunya.com/custom/path"),
      URL(string: "https://search-dev.shunya.com/custom/path"),
      URL(string: "https://community.shunya.com"),
      URL(string: "https://subdomain.shunya.com"),
      URL(string: "https://shunya.com"),
    ].compactMap { $0 }

    badURLs.forEach {
      XCTAssertNotEqual(DomainUserScript(for: $0), .shunyaTalkHelper)
    }
  }
}
